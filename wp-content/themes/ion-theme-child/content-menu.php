<p><?php the_content(); ?></p>

<!--- *****************************
      Starter Menu Items
****************************** --->

<?php $posts = get_field('starters'); ?>

<h3><span style="color:#d55731">Starters</span></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

        <h3><?php the_title(); ?><span style="float:right;margin-right:10px;">$<?php the_field ('price'); ?></span></h3>
        <?php the_content(); ?>

    <?php endforeach; ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>
<hr>
<!--- *****************************
      Salad Menu Items
****************************** --->

<?php $posts = get_field('salads'); ?>

<h3><span style="color:#d55731">Salads</span></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

        <h3><?php the_title(); ?><span style="float:right;margin-right:10px;">$<?php the_field ('price'); ?></span></h3>
        <?php the_content(); ?>

    <?php endforeach; ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>
<hr>
<!--- *****************************
      BBQ Sandwiches Menu Items
****************************** --->

<?php $posts = get_field('bbq_sandwiches'); ?>

<h3><span style="color:#d55731">BBQ Sandwiches</span></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

        <h3><?php the_title(); ?><span style="float:right;margin-right:10px;">$<?php the_field ('price'); ?></span></h3>
        <?php the_content(); ?>

    <?php endforeach; ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>
<hr>
<!--- *****************************
      Burger Menu Items
****************************** --->

<?php $posts = get_field('burgers'); ?>

<h3><span style="color:#d55731">Burgers</span></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

        <h3><?php the_title(); ?><span style="float:right;margin-right:10px;">$<?php the_field ('price'); ?></span></h3>
        <?php the_content(); ?>

    <?php endforeach; ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>
<hr>
<!--- *****************************
      Ribs & BBQ Plate Menu Items
****************************** --->

<?php $posts = get_field('ribs_bbq_plates'); ?>

<h3><span style="color:#d55731">Ribs &amp; BBQ Plates</span></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

        <h3><?php the_title(); ?><span style="float:right;margin-right:10px;">$<?php the_field ('price'); ?></span></h3>
        <?php the_content(); ?>

    <?php endforeach; ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>
<hr>
<!--- *****************************
      Vegetarian Menu Items
****************************** --->

<?php $posts = get_field('vegetarian'); ?>

<h3><span style="color:#d55731">Vegetarian</span></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

        <h3><?php the_title(); ?><span style="float:right;margin-right:10px;">$<?php the_field ('price'); ?></span></h3>
        <?php the_content(); ?>

    <?php endforeach; ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>
<hr>
<!--- *****************************
      Kids Meal Menu Items
****************************** --->

<?php $posts = get_field('kids_meals'); ?>

<h3><span style="color:#d55731">Kid's Meals</span></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

        <h3><?php the_title(); ?><span style="float:right;margin-right:10px;">$<?php the_field ('price'); ?></span></h3>
        <?php the_content(); ?>

    <?php endforeach; ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>
<hr>
<!--- *****************************
      Homemade Desserts Menu Items
****************************** --->

<?php $posts = get_field('homemade_desserts'); ?>

<h3><span style="color:#d55731">Homemade Desserts</span></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

        <h3><?php the_title(); ?><span style="float:right;margin-right:10px;">$<?php the_field ('price'); ?></span></h3>
        <?php the_content(); ?>

    <?php endforeach; ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>
<hr>
<!--- *****************************
      Beverage Menu Items
****************************** --->

<?php $posts = get_field('beverages'); ?>

<h3><span style="color:#d55731">Beverages</span></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

        <h3><?php the_title(); ?><span style="float:right;margin-right:10px;">$<?php the_field ('price'); ?></span></h3>
        <?php the_content(); ?>

    <?php endforeach; ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>
<hr>
<!--- *****************************
      Bulk Take Out Menu Items
****************************** --->

<?php $posts = get_field('bulk_takeout'); ?>

<h3><span style="color:#d55731">Bulk Takeout</span></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

        <h3><?php the_title(); ?><span style="float:right;margin-right:10px;">$<?php the_field ('price'); ?></span></h3>
        <?php the_content(); ?>

    <?php endforeach; ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>
<hr>
