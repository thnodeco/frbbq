<?php

// Enqueue Ion parent theme CSS file

add_action( 'wp_enqueue_scripts', 'appp_ion_enqueue_styles' );
function appp_ion_enqueue_styles() {

	// parent theme css
	$version = AppPresser_Ion_Theme_Setup::VERSION . '.' . filemtime( get_template_directory() . '/style.css' );
    wp_enqueue_style( 'ion-style', get_template_directory_uri().'/style.css', null, $version );

    // child theme css
    wp_enqueue_style( 'ion-child-style', get_stylesheet_uri(), null, filemtime( get_stylesheet_directory() . '/style.css' ) );
}

// Add your custom functions here

/*
 * Set cookie on first visit
 */

function app_is_first_time() {

    if ( isset($_COOKIE['_wp_first_time_new7']) || is_user_logged_in() ) {
        return false;
    } else {
        // expires in 30 days, you may want to change this
        setcookie('_wp_first_time_new7', 1, time() + (WEEK_IN_SECONDS * 4), COOKIEPATH, COOKIE_DOMAIN, false);

        return true;
    }
}

add_action( 'init', 'app_is_first_time' );

/*
 * Show intro screen if it's a first time visit, or user is not logged in
 */

function app_show_intro() {

    $path=$_SERVER['REQUEST_URI'];

    if( strpos($path, 'intro') == true || is_user_logged_in() || isset( $_COOKIE['_wp_first_time_new7'] ) )
        return;

    wp_redirect( 'http://developmentsite.review/DBS/frbbq/app-home/intro' );
    exit;
}

add_action( 'init', 'app_show_intro', 999 );
