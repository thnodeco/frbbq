

<p><?php the_content(); ?></p>

<!--- *****************************
       On Tap Beers
****************************** --->

<?php $posts = get_field('on_tap'); ?>

<h3>On Tap</h3>
<hr>
<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

            <h3><?php the_title(); ?><span style="float:right;margin-right:10px;">$<?php the_field ('glass-price'); ?><?php the_field ('bottle-price'); ?><?php the_field ('can-price'); ?></span></h3>
            <h5>Draft Beers<span style="float:right;margin-right:10px;">ABV: <?php the_field ('abv'); ?></span><br />
              <?php the_field ('type'); ?>: <?php the_field ('glass_size'); ?><?php the_field ('bottle_size'); ?><?php the_field ('can_size'); ?><span style="float:right;margin-right:10px;">IBU: <?php the_field ('ibu'); ?></span></h5>
            <br />
            <?php the_content(); ?>
            <hr>

    <?php endforeach; ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>

<!--- *****************************
       Common Beers
****************************** --->

<?php $posts = get_field('common_beers'); ?>

<h3><em>Standard Beers</em></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

            <h3><?php the_title(); ?><span style="float:right;margin-right:10px;">$<?php the_field ('glass-price'); ?><?php the_field ('bottle-price'); ?><?php the_field ('can-price'); ?></span></h3>
            <h5><?php the_field ('type'); ?>: <?php the_field ('glass_size'); ?><?php the_field ('bottle_size'); ?><?php the_field ('can_size'); ?></h5>
            <br />
            <?php the_content(); ?>

    <?php endforeach; ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>
