

<p><?php the_content(); ?></p>

<!--- *****************************
       Specials
****************************** --->


  <?php $specials=new WP_Query(array( 'post_type'=> 'menu_items', 'menu_category' => 'specials', 'post_per_page' => '6', )); ?>

    <h1>Our daily specials are delicious.</h1>

      <?php while ($specials->have_posts()) : $specials->the_post(); ?>

        <h3><?php the_title(); ?><span style="float:right;margin-right:10px;">$<?php the_field ('price'); ?></span></h3>
        <p><?php the_content(); ?></p>

      <?php endwhile; ?>
  <?php wp_reset_query(); ?>
