<?php
/**
 * Template Name: Beer Template
 */
?>

<?php while (have_posts()) : the_post(); ?><div class="col-md-10 col-md-offset-1">
<?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'beer'); ?></div>
<?php endwhile; ?>
