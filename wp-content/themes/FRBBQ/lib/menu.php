<?php
// Register Menu Items Custom Post Type
function menu_items() {

	$labels = array(
		'name'                  => _x( 'Menu Items', 'Post Type General Name', 'frbbq_menu_items' ),
		'singular_name'         => _x( 'Menu', 'Post Type Singular Name', 'frbbq_menu_items' ),
		'menu_name'             => __( 'Menu Items', 'frbbq_menu_items' ),
		'name_admin_bar'        => __( 'Menu Items', 'frbbq_menu_items' ),
		'archives'              => __( 'Menu Item Archives', 'frbbq_menu_items' ),
		'parent_item_colon'     => __( 'Parent Menu Item:', 'frbbq_menu_items' ),
		'all_items'             => __( 'All Menu Items', 'frbbq_menu_items' ),
		'add_new_item'          => __( 'Add New Menu Items', 'frbbq_menu_items' ),
		'add_new'               => __( 'Add New Menu Items', 'frbbq_menu_items' ),
		'new_item'              => __( 'New Menu Items', 'frbbq_menu_items' ),
		'edit_item'             => __( 'Edit Menu Items', 'frbbq_menu_items' ),
		'update_item'           => __( 'Update Menu Items', 'frbbq_menu_items' ),
		'view_item'             => __( 'View Menu Items', 'frbbq_menu_items' ),
		'search_items'          => __( 'Search Menu Items', 'frbbq_menu_items' ),
		'not_found'             => __( 'Not found', 'frbbq_menu_items' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'frbbq_menu_items' ),
		'featured_image'        => __( 'Featured Image', 'frbbq_menu_items' ),
		'set_featured_image'    => __( 'Set featured image', 'frbbq_menu_items' ),
		'remove_featured_image' => __( 'Remove featured image', 'frbbq_menu_items' ),
		'use_featured_image'    => __( 'Use as featured image', 'frbbq_menu_items' ),
		'insert_into_item'      => __( 'Insert into menu item', 'frbbq_menu_items' ),
		'uploaded_to_this_item' => __( 'Uploaded to this menu item', 'frbbq_menu_items' ),
		'items_list'            => __( 'Menu Items list', 'frbbq_menu_items' ),
		'items_list_navigation' => __( 'Menu Items list navigation', 'frbbq_menu_items' ),
		'filter_items_list'     => __( 'Filter items list', 'frbbq_menu_items' ),
	);
	$args = array(
		'label'                 => __( 'Menu', 'frbbq_menu_items' ),
		'description'           => __( 'FRBBQ Menu Items', 'frbbq_menu_items' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', ),
		'taxonomies'            => array( 'menu_category', 'menu_items' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-store',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'menu_items', $args );

}
add_action( 'init', 'menu_items', 0 );

if ( ! function_exists( 'menu_taxonomy' ) ) {

// Register Custom Taxonomy
function menu_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Menu Categories', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Menu Category', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Menu Categories', 'text_domain' ),
		'all_items'                  => __( 'All Menu Categories', 'text_domain' ),
		'parent_item'                => __( 'Parent Menu Category', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Menu Category:', 'text_domain' ),
		'new_item_name'              => __( 'New Menu Category', 'text_domain' ),
		'add_new_item'               => __( 'Add New Menu Category', 'text_domain' ),
		'edit_item'                  => __( 'Edit Menu Category', 'text_domain' ),
		'update_item'                => __( 'Update Menu Category', 'text_domain' ),
		'view_item'                  => __( 'View Menu Category', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate menu categories with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove menu categories', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Menu Categories', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No menu categories', 'text_domain' ),
		'items_list'                 => __( 'Menu category list', 'text_domain' ),
		'items_list_navigation'      => __( 'Menu category navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'menu_category', array( 'menu_items' ), $args );

}
add_action( 'init', 'menu_taxonomy', 0 );

}


?>
