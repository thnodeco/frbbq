<?php
// Register Beer List Custom Post Type
function beer_list() {

	$labels = array(
		'name'                  => _x( 'Beer List', 'Post Type General Name', 'frbbq_beer_list' ),
		'singular_name'         => _x( 'Beers', 'Post Type Singular Name', 'frbbq_beer_list' ),
		'menu_name'             => __( 'Beer Database', 'frbbq_beer_list' ),
		'name_admin_bar'        => __( 'Beers', 'frbbq_beer_list' ),
		'archives'              => __( 'Beer List Archives', 'frbbq_beer_list' ),
		'parent_item_colon'     => __( 'Parent Beer List:', 'frbbq_beer_list' ),
		'all_items'             => __( 'All Beers', 'frbbq_beer_list' ),
		'add_new_item'          => __( 'Add New Beer', 'frbbq_beer_list' ),
		'add_new'               => __( 'Add New Beer', 'frbbq_beer_list' ),
		'new_item'              => __( 'New Beers', 'frbbq_beer_list' ),
		'edit_item'             => __( 'Edit Beer', 'frbbq_beer_list' ),
		'update_item'           => __( 'Update Beer', 'frbbq_beer_list' ),
		'view_item'             => __( 'View Beer', 'frbbq_beer_list' ),
		'search_items'          => __( 'Search Beer List', 'frbbq_beer_list' ),
		'not_found'             => __( 'Not found', 'frbbq_beer_list' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'frbbq_beer_list' ),
		'featured_image'        => __( 'Featured Image', 'frbbq_beer_list' ),
		'set_featured_image'    => __( 'Set featured image', 'frbbq_beer_list' ),
		'remove_featured_image' => __( 'Remove featured image', 'frbbq_beer_list' ),
		'use_featured_image'    => __( 'Use as featured image', 'frbbq_beer_list' ),
		'insert_into_item'      => __( 'Insert into beer', 'frbbq_beer_list' ),
		'uploaded_to_this_item' => __( 'Uploaded to this beer', 'frbbq_beer_list' ),
		'items_list'            => __( 'Beer list', 'frbbq_beer_list' ),
		'items_list_navigation' => __( 'Beer list navigation', 'frbbq_beer_list' ),
		'filter_items_list'     => __( 'Filter beer list', 'frbbq_beer_list' ),
	);
	$args = array(
		'label'                 => __( 'Beer List', 'frbbq_beer_list' ),
		'description'           => __( 'FRBBQ Beer List', 'frbbq_beer_list' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', ),
		'taxonomies'            => array( 'beer_category', 'beer_list' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-smiley',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'beer_list', $args );

}
add_action( 'init', 'beer_list', 0 );

if ( ! function_exists( 'beer_taxonomy' ) ) {

// Register Custom Taxonomy
function beer_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Beer Categories', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Beer Category', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Beer Categories', 'text_domain' ),
		'all_items'                  => __( 'All Beer Categories', 'text_domain' ),
		'parent_item'                => __( 'Parent Beer Category', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Beer Category:', 'text_domain' ),
		'new_item_name'              => __( 'New Beer Category', 'text_domain' ),
		'add_new_item'               => __( 'Add New Beer Category', 'text_domain' ),
		'edit_item'                  => __( 'Edit Beer Category', 'text_domain' ),
		'update_item'                => __( 'Update Beer Category', 'text_domain' ),
		'view_item'                  => __( 'View Beer Category', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate beer categories with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove beer categories', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Beer Categories', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No beer categories', 'text_domain' ),
		'items_list'                 => __( 'beer category list', 'text_domain' ),
		'items_list_navigation'      => __( 'beer category navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'beer_category', array( 'beer_list' ), $args );

}
add_action( 'init', 'beer_taxonomy', 0 );

}


if ( ! function_exists( 'brewer_taxonomy' ) ) {

// Register Custom Taxonomy
function brewer_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Breweries', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Brewery', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Breweries', 'text_domain' ),
		'all_items'                  => __( 'All Breweries', 'text_domain' ),
		'parent_item'                => __( 'Parent Brewery', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Brewery:', 'text_domain' ),
		'new_item_name'              => __( 'New Brewery', 'text_domain' ),
		'add_new_item'               => __( 'Add Brewery', 'text_domain' ),
		'edit_item'                  => __( 'Edit Brewery', 'text_domain' ),
		'update_item'                => __( 'Update Brewery', 'text_domain' ),
		'view_item'                  => __( 'View Brewery', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate breweries with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove breweries', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Breweries', 'text_domain' ),
		'search_items'               => __( 'Search Brewery', 'text_domain' ),
		'not_found'                  => __( 'Brewery Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No breweries', 'text_domain' ),
		'items_list'                 => __( 'Brewery list', 'text_domain' ),
		'items_list_navigation'      => __( 'Breweries list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'brewer_taxonomy', array( 'beer_list' ), $args );

}
add_action( 'init', 'brewer_taxonomy', 0 );

}

?>
