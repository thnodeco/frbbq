<?php /** * Template Name: Menu Item Template */ ?>

<?php while (have_posts()) : the_post(); ?>
<div class="col-md-10 col-md-offset-1">
    <?php get_template_part( 'templates/page', 'header'); ?>

<?php get_template_part( 'templates/content', 'menu'); ?></div>
<?php endwhile; ?>