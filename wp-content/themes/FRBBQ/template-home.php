<?php /** * Template Name: Home Page Template */ ?>

<?php while (have_posts()) : the_post(); ?>
<div class="col-md-10 col-md-offset-1">
    <?php get_template_part( 'templates/page', 'header'); ?>
</div>
<?php get_template_part( 'templates/content', 'home'); ?>
<?php endwhile; ?>