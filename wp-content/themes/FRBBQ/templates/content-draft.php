<div class="col-sm-6" style="margin-right: 35px;padding-right: 20px;">
    <div class="row border">

        <img src="../wp-content/uploads/2016/02/logo.png" width="30%" style="display: inline-block; float: left" />
        <h1 style="display: inline-block; float: left; width: 60%; margin-left: 20px; margin-top: 10px;">Specialty<br/ >Drafts</h1>
        <section>

            <ul class="leaders">
                <?php $page_id=8 ; $posts=get_field( 'on_tap', $page_id); ?>
                <?php if( $posts ): ?>
                <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                <?php setup_postdata($post); ?>
                <li class="drink">
                    <span style="color:#981f02 !important;font-size: 2.2em !important;"><?php the_title(); ?></span>
                    <span  style="color:#981f02 !important; font-size: 2.2em !important;">$<?php the_field ('glass-price'); ?><div style="color:#d55731 !important;font-size: .6em !important; ">ABV: <?php the_field ('abv'); ?><?php if ( get_field( 'ibu' ) == "" ): ?><?php else: // field_name returned false ?></div><div style="color:#572314 !important;font-size: .8em !important; font-weight: 300 !important;font-style: italic !important;font-size: .6em !important; ">IBU:<?php the_field ( 'ibu' ); ?><?php endif; // end of if field_name logic ?></div></span>
                    <div class="details" style="font-size: 1.35em !important;">
                        <?php $brewer=get_the_term_list( $post->ID, 'brewer_taxonomy', '', ', ', ''); echo strip_tags($brewer); ?>
                        <div style="color: #572314 !important;margin-bottom: 5px;  font-style: italic !important;  font-weight: 300 !important;"><?php the_field ('type'); ?> | Draft Beers: <?php the_field ('glass_size'); ?><?php the_field ('bottle_size'); ?><?php the_field ('can_size'); ?></div>
                     
                    </div>
                </li>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                <?php endif; ?>
                            </ul>
        </section>


        
    </div>
</div>

<div class="col-sm-6" style="padding-left: 20px;">
    <div class="row border">

        <img src="../wp-content/uploads/2016/02/logo.png" width="30%" style="display: inline-block; float: left" />
        <h1 style="display: inline-block; float: left; width: 60%; margin-left: 20px; margin-top: 0px;">Specialty<br/ >Drafts</h1>
        <section>

            <ul class="leaders">
                <?php $page_id=8 ; $posts=get_field( 'on_tap', $page_id); ?>
                <?php if( $posts ): ?>
                <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                <?php setup_postdata($post); ?>
                <li class="drink">
                    <span style="color:#981f02 !important;font-size: 2.2em !important;"><?php the_title(); ?></span>
                    <span  style="color:#981f02 !important; font-size: 2.2em !important;">$<?php the_field ('glass-price'); ?><div style="color:#d55731 !important;font-size: .6em !important; ">ABV: <?php the_field ('abv'); ?><?php if ( get_field( 'ibu' ) == "" ): ?><?php else: // field_name returned false ?></div><div style="color:#572314 !important;font-size: .8em !important; font-weight: 300 !important;font-style: italic !important;font-size: .6em !important; ">IBU:<?php the_field ( 'ibu' ); ?><?php endif; // end of if field_name logic ?></div></span>
                    <div class="details" style="font-size: 1.35em !important;">
                        <?php $brewer=get_the_term_list( $post->ID, 'brewer_taxonomy', '', ', ', ''); echo strip_tags($brewer); ?>
                        <div style="color: #572314 !important;margin-bottom: 5px;  font-style: italic !important;  font-weight: 300 !important;"><?php the_field ('type'); ?> | Draft Beers: <?php the_field ('glass_size'); ?><?php the_field ('bottle_size'); ?><?php the_field ('can_size'); ?></div>
                     
                    </div>
                </li>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                <?php endif; ?>
                            </ul>
        </section>


        
    </div>
</div>
