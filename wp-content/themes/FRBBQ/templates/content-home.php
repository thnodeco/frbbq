<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <?php the_content(); ?>
        </div>
    </div>
</div>

<div class="container-fluid full box-shadow">
    <div class="row cta">
        <div class="col-md-8 col-md-offset-1">
            <span class="cta-text"><?php the_field( 'cta_text'); ?></span>

        </div>
        <div class="col-md-1">
            <button type="button" class="btn blk-btn">
                <a href="<?php the_field('cta_link'); ?>">
                    <?php the_field( 'cta_button_text'); ?>
                </a>
            </button>
        </div>
    </div>
</div>

<div class="container-fluid full">
    <div class="row serious">
        <div class="col-md-10 col-md-offset-1">
            <section class="rw-wrapper">
                <h2 class="rw-sentence">
  		       <span>We're serious about our </span>
          		<div class="rw-words rw-words-1">
          			<span>beer</span>
          			<span>BBQ</span>
          			<span>music</span>
          		</div>
  	      </h2>
            </section>
            <div class="row moveup">
                <div class="col-md-8">
                    <p>It’s not just BBQ that we love. We get excited about great craft beers. Our rotating tap and bottle list are always filled with unique, hard to find, and satisfying brews.</p>

                    <p>We do beer tappings and special events on a regular basis. Be sure to download our mobile app to stay connected to the latest happenings and to catch your favorite craft beer when it comes to our restaurant.</p>
 <p>We offer a full-service bar and home-cooked meals, made from scratch. Bring your friends and watch the game, or unwind and relax to our great live music Wednesday, Saturday, and Sunday evenings. We welcome you to our House, where we love Good Food, Good Music, Good Beer, and Good Times with our Friends &amp; Family!</p>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".card-grid").flip({
            axis: 'y',
            trigger: 'hover'
        });
    });
</script>

<div class="container-fluid full">
    <div class="specials row">

        <?php $specials=new WP_Query(array( 'post_type'=> 'menu_items', 'menu_category' => 'specials', 'post_per_page' => '6', )); ?>

        <h1>Our daily specials are delicious.</h1>

        <?php while ($specials->have_posts()) : $specials->the_post(); ?>

        <div class="col-sm-12 col-md-3 card-grid">
          <div class="row">
            <div class="front">
                <h3 style="font-size: 2em"><a href="<?php the_permalink(); ?>"><?php the_title(); ?>
 						<?php if ( get_field( 'type' ) == "Half Rack / Full Rack" ): ?>

              <br />Half $<?php the_field ('half_rack_price'); ?><br />Full $<?php the_field ('full_rack_price'); ?></a></h3>

            <?php elseif ( get_field( 'type' ) == "Cup / Bowl" ): ?>

              <br />Cup $<?php the_field ('cup_price'); ?><br />Bowl $<?php the_field ('bowl_price'); ?></a></h3>

            <?php else: // field_name returned false ?>

              - $<?php the_field ('price'); ?></a></h3>

            <?php endif; // end of if field_name logic ?>

                <p>
                    <?php the_content(); ?>
                </p>
            </div>
            <div class="back">
                <?php the_post_thumbnail(); ?>
            </div>
          </div>
        </div>

        <?php endwhile; ?>
        <?php wp_reset_query(); ?>

    </div>

</div>

<div class="container-fluid full">
    <div class="row dark">
        <div class="col-md-6 col-md-offset-1 light" style="margin-left: 0px;padding:30px 50px;">
            <?php echo do_shortcode( '[testimonials_cycle theme="default_style" category="homepage" count="5" order_by="date" order="ASC" show_title="1" hide_view_more="0" testimonials_per_slide="1" transition="fade" timer="9000" pause_on_hover="true" auto_height="calc" show_pager_icons="1" prev_next="1"]' ); ?>
        </div>
        <div class="col-md-4 col-md-offset-1">

            <!-- Begin MailChimp Signup Form -->

            <div id="mc_embed_signup">
            <form action="//frbbq.us12.list-manage.com/subscribe/post?u=7482017b7548b9539f52f75e6&amp;id=9f08d9149d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                <h2>Don't miss a thing!</h2>
            <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
            <div class="mc-field-group">
                <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
            </label>
                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
            </div>
            <div class="mc-field-group">
                <label for="mce-FNAME">First Name </label>
                <input type="text" value="" name="FNAME" class="" id="mce-FNAME">
            </div>
            <div class="mc-field-group">
                <label for="mce-LNAME">Last Name </label>
                <input type="text" value="" name="LNAME" class="" id="mce-LNAME">
            </div>
            <div class="mc-field-group">
                <label for="mce-MMERGE3">Birthday / Month </label>
                <input type="text" value="" name="MMERGE3" class="" id="mce-MMERGE3">
            </div>
            <div class="mc-field-group">
                <label for="mce-MMERGE4">Zip/Postal Code - Home </label>
                <input type="text" value="" name="MMERGE4" class="" id="mce-MMERGE4">
            </div>
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_7482017b7548b9539f52f75e6_9f08d9149d" tabindex="-1" value=""></div>
                <div class="clear"><input type="submit" value="Get our Newsletter" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                </div>
            </form>
            </div>
            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='MMERGE3';ftypes[3]='text';fnames[4]='MMERGE4';ftypes[4]='zip';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
            <!--End mc_embed_signup-->
        </div>

    </div>
</div>

<div class="container-fluid full">
    <div class="row eventsdisplay">
        <h1>Upcoming Events</h1>
<?php
  global $events;
  $args = array(
  		'start_date' => current_time( 'Y-m-d hh:mm' ),
  		'posts_per_page' => 1,
  		'ignore_sticky_posts' => true
  );

  //	This grabs all the events based on the $fe_args
  $events = tribe_get_events($args);
  foreach ( $events as $post ) {
  	setup_postdata( $post );

  	?>
  	<div class='fe-wrap'>
  		<!-- This lays out the featured image -->
  		<div class='col-md-4 fe-image'>
  			<?php echo tribe_event_featured_image(null, array(450,450)); ?>
  		</div>
  		<!-- This lays out the event title -->
  		<div class='col-md-8 fe-title'>
  			<a class="tribe-event-url" href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
  				<h3><?php the_title() ?></h3>
  			</a>
  		</div>
  		<!-- This lays out the event schedule -->
  		<div class="col-md-8 fe-date">
  			<h2><?php echo tribe_events_event_schedule_details() . '<br/>'; ?></h2>
        <?php the_content() ?>
  		</div>
  	</div>

  	<?php
  }
?>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $(".gallery a").lightbox();

    });
</script>

<div class="container-fluid full">
    <div class="row fun">
        <h1>We have fun. Lots of it.</h1>
        <p>Front Range BBQ is an invested part of the local community. We host regular events, local art shows, and live music featuring everything from Bluegrass to Blues. Check out our Wednesday, Saturday, and Sunday evening music scene, where we feature original live music from local Colorado Springs musicians and national touring acts.</p>

        <p>There is always something exciting going on at our Old Colorado City location. Check out some of these great images from guests and staff from our prior events and see all the fun that we have to offer.</p>

      <a href ="http://devtest.frbbq.com/gallery/" /><h3>SEE OUR PHOTO GALLERY</h3></a>

        <div class="row">
            <div class="col-md-4 gallery">
                <a href="wp-content/uploads/2016/03/IMG_1824.jpg" rel="lightbox" title="Gallery 1"><img src="wp-content/uploads/2016/03/IMG_1824.jpg" width="400">
                </a>
            </div>
            <div class="col-md-4 gallery">
                <a href="wp-content/uploads/2016/03/IMG_2225.jpg" rel="lightbox" title="Gallery 2"><img src="wp-content/uploads/2016/03/IMG_2225.jpg" width="400">
                </a>
            </div>
            <div class="col-md-4 gallery">
                <a href="wp-content/uploads/2016/03/P1030854.jpg" rel="lightbox" title="Gallery 3"><img src="wp-content/uploads/2016/03/P1030854.jpg" width="400">
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 gallery">
                <a href="wp-content/uploads/2016/03/P1020359.jpg" rel="lightbox" title="Gallery 4"><img src="wp-content/uploads/2016/03/P1020359.jpg" width="400">
                </a>
            </div>
            <div class="col-md-4 gallery">
                <a href="wp-content/uploads/2016/03/IMG_1894.jpg" rel="lightbox" title="Gallery 5"><img src="wp-content/uploads/2016/03/IMG_1894.jpg" width="400">
                </a>
            </div>
            <div class="col-md-4 gallery">
                <a href="wp-content/uploads/2016/03/DSC_7806.jpg" rel="lightbox" title="Gallery 6"><img src="wp-content/uploads/2016/03/DSC_7806.jpg" width="400">
                </a>
            </div>
        </div>

    </div>
</div>

<?php wp_link_pages([ 'before'=> '
<nav class="page-nav">
    <p>' . __('Pages:', 'sage'), 'after' => '</p>
</nav>']); ?>
