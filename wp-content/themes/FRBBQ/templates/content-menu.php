<p><?php the_content(); ?></p>

<!--- *****************************
      Featured Menu Item
****************************** --->

<?php $featured = new WP_Query(array(
  'post_type' => 'menu_items',
  'menu_category' => 'featured',
  'post_per_page' => '1',
)); ?>

<div class="clearfix"></div>

          <div class="featured-item">
          <?php while ($featured->have_posts()) : $featured->the_post(); ?>
            <h2><em><?php the_title(); ?> - $<?php the_field ('price'); ?></em></h2>
              <div class="image">
                  <?php the_post_thumbnail(); ?>
              </div>
            <span style="color:#fffeee"><?php the_content(); ?></span>
          </div>

<?php endwhile; ?> <?php wp_reset_query(); ?>


<!--- *****************************
      Starter Menu Items
****************************** --->

<?php $posts = get_field('starters'); ?>

<h3><em>Starters</em></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

<div class="row">

        <div class="col-md-6">
            <div class="col-md-9">
                <h3><?php the_title(); ?></h3>
                <?php the_content(); ?>
            </div>
            <div class="col-md-3">

            <?php if ( get_field( 'type' ) == "Cup / Bowl" ): ?>

              <h3>Cup $<?php the_field ('cup_price'); ?><br />Bowl $<?php the_field ('bowl_price'); ?></h3>

            <?php else: // field_name returned false ?>

              <h3>$<?php the_field ('price'); ?></h3>

            <?php endif; // end of if field_name logic ?>

            </div>
        </div>

    <?php endforeach; ?>
</div>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>

<!--- *****************************
      Salad Menu Items
****************************** --->

<?php $posts = get_field('salads'); ?>

<h3><em>Salads</em></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

<div class="row">

        <div class="col-md-6">
            <div class="col-md-9">
                <h3><?php the_title(); ?></h3>
                <?php the_content(); ?>
            </div>
            <div class="col-md-3">
            <?php if ( get_field( 'type' ) == "Salad" ): ?>

              <h3>Small $<?php the_field ('small_salad_price'); ?><br />Large $<?php the_field ('large_salad_price'); ?></h3>

            <?php else: // field_name returned false ?>

              <h3>$<?php the_field ('price'); ?></h3>

            <?php endif; // end of if field_name logic ?>
            </div>
        </div>

    <?php endforeach; ?>
</div>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>

<!--- *****************************
      BBQ Sandwiches Menu Items
****************************** --->

<?php $posts = get_field('bbq_sandwiches'); ?>

<h3><em>BBQ Sandwiches</em></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

<div class="row">

        <div class="col-md-6">
            <div class="col-md-9">
                <h3><?php the_title(); ?></h3>
                <?php the_content(); ?>
            </div>
            <div class="col-md-3">
                <h3>$<?php the_field ('price'); ?></h3>
            </div>
        </div>

    <?php endforeach; ?>
</div>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>

<!--- *****************************
      Burger Menu Items
****************************** --->

<?php $posts = get_field('burgers'); ?>

<h3><em>Burgers</em></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

<div class="row">

        <div class="col-md-6">
            <div class="col-md-9">
                <h3><?php the_title(); ?></h3>
                <?php the_content(); ?>
            </div>
            <div class="col-md-3">
                <h3>$<?php the_field ('price'); ?></h3>
            </div>
        </div>

    <?php endforeach; ?>
</div>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>

<!--- *****************************
      Ribs & BBQ Plate Menu Items
****************************** --->

<?php $posts = get_field('ribs_bbq_plates'); ?>

<h3><em>Ribs &amp; BBQ Plates</em></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

<div class="row">

        <div class="col-md-6">
            <div class="col-md-9">
                <h3><?php the_title(); ?></h3>
                <?php the_content(); ?>
            </div>
            <div class="col-md-3">
            <?php if ( get_field( 'type' ) == "Half Rack / Full Rack" ): ?>

              <h3>Half $<?php the_field ('half_rack_price'); ?><br />Full $<?php the_field ('full_rack_price'); ?></h3>

            <?php else: // field_name returned false ?>

              <h3>$<?php the_field ('price'); ?></h3>

            <?php endif; // end of if field_name logic ?>
            </div>
        </div>

    <?php endforeach; ?>
</div>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>

<!--- *****************************
      Vegetarian Menu Items
****************************** --->

<?php $posts = get_field('vegetarian'); ?>

<h3><em>Vegetarian</em></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

<div class="row">

        <div class="col-md-6">
            <div class="col-md-9">
                <h3><?php the_title(); ?></h3>
                <?php the_content(); ?>
            </div>
            <div class="col-md-3">
                <h3>$<?php the_field ('price'); ?></h3>
            </div>
        </div>

    <?php endforeach; ?>
</div>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>

<!--- *****************************
      Kids Meal Menu Items
****************************** --->

<?php $posts = get_field('kids_meals'); ?>

<h3><em>Kid's Meals</em></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

<div class="row">

        <div class="col-md-6">
            <div class="col-md-9">
                <h3><?php the_title(); ?></h3>
                <?php the_content(); ?>
            </div>
            <div class="col-md-3">
                <h3>$<?php the_field ('price'); ?></h3>
            </div>
        </div>

    <?php endforeach; ?>
</div>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>

<!--- *****************************
      Homemade Desserts Menu Items
****************************** --->

<?php $posts = get_field('homemade_desserts'); ?>

<h3><em>Homemade Desserts</em></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

<div class="row">

        <div class="col-md-6">
            <div class="col-md-9">
                <h3><?php the_title(); ?></h3>
                <?php the_content(); ?>
            </div>
            <div class="col-md-3">
            <?php if ( get_field( 'type' ) == "Pie" ): ?>

              <h3>Slice $<?php the_field ('slice_price_so_nice'); ?><br />Whole $<?php the_field ('whole_price'); ?></h3>

            <?php else: // field_name returned false ?>

              <h3>$<?php the_field ('price'); ?></h3>

            <?php endif; // end of if field_name logic ?>
            </div>
        </div>

    <?php endforeach; ?>
</div>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>

<!--- *****************************
      Beverage Menu Items
****************************** --->

<?php $posts = get_field('beverages'); ?>

<h3><em>Beverages</em></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

<div class="row">

        <div class="col-md-6">
            <div class="col-md-9">
                <h3><?php the_title(); ?></h3>
                <?php the_content(); ?>
            </div>
            <div class="col-md-3">
                <h3>$<?php the_field ('price'); ?></h3>
            </div>
        </div>

    <?php endforeach; ?>
</div>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>

<!--- *****************************
      Bulk Take Out Menu Items
****************************** --->

<?php $posts = get_field('bulk_takeout'); ?>

<h3><em>Bulk Takeout</em></h3>

<?php if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>

<div class="row">

        <div class="col-md-6">
            <div class="col-md-9">
                <h3><?php the_title(); ?></h3>
                <?php the_content(); ?>
            </div>
            <div class="col-md-3">

              <?php if ( get_field( 'type' ) == "Pie" ): ?>

              <h3>Slice $<?php the_field ('slice_price_so_nice'); ?><br />Whole $<?php the_field ('whole_price'); ?></h3>

            <?php elseif ( get_field( 'type' ) == "Half Rack / Full Rack" ): ?>

              <h3>Half Rack $<?php the_field ('half_rack_price'); ?><br />Full Rack $<?php the_field ('full_rack_price'); ?></h3>

                <?php else: // field_name returned false ?>

              <h3>$<?php the_field ('price'); ?></h3>

            <?php endif; // end of if field_name logic ?>
            </div>
        </div>

    <?php endforeach; ?>
</div>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>

    <div class="clearfix"><br /><br /></div>
