<footer class="content-info">
    <div class="container-fluid" style="padding-bottom:30px;">
        <div class="col-sm-12">
            <?php dynamic_sidebar( 'sidebar-footer'); ?>
        </div>
    </div>
    <div class="copyright row" style="margin-bottom:0;">
        <p class="col-md-6">Copyright 2016 | Front Range Barbeque</p>
        <p class="col-md-6" style="text-align:right;">
          Design by: <a href="http://www.designbreakstudios.com" target="_blank" title="Design Break Studios">Design Break Studios</a>
          Powered by: <a href="http://www.9thnode.com" target="_blank" title="9th Node Networks">9th Node Networks</a></p>
    </div>
</footer>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-78329401-1', 'auto');
  ga('send', 'pageview');

</script>
