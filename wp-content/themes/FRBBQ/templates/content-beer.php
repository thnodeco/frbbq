<p>
    <?php the_content(); ?>
</p>

<!--- *****************************
       On Tap Beers
****************************** --->

<?php $posts=get_field( 'on_tap'); ?>
<div class="row">
<h2><em>On Tap</em></h2>

<?php if( $posts ): ?>
<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
<?php setup_postdata($post); ?>




        <div class="col-md-9">
            <h3><?php if ( get_field( 'gluten_free' ) ): ?><img src="../wp-content/themes/FRBBQ/dist/images/gluten-free.png" />
            <?php endif; // end of if field_name logic ?><?php the_title(); ?></h3>
            <h5><?php $brewer = get_the_term_list( $post->ID, 'brewer_taxonomy', '', ', ', ''); echo strip_tags($brewer); ?></h5>
            <h5><?php the_field ('type'); ?>: <?php the_field ('glass_size'); ?><?php the_field ('bottle_size'); ?><?php the_field ('can_size'); ?> glass</h5>
        </div>
        <div class="col-md-3">
            <h3>$<?php the_field ('glass-price'); ?><?php the_field ('bottle_price'); ?><?php the_field ('can_price'); ?></h3>
            <h5>ABV: <?php the_field ('abv'); ?><br /> IBU: <?php the_field ('ibu'); ?></h5>
        </div>
        <div class="col-md-12">
            <?php the_content(); ?>
        </div>
  <hr class="col-md-12">

    <?php endforeach; ?>
</div>

<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>


<!--- *****************************
       Craft Beers
****************************** --->

<?php $posts=get_field( 'craft_beers'); ?>
<div class="row">
<h2><em>Specialty Bottled/Can Beers</em></h2>

<?php if( $posts ): ?>
<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
<?php setup_postdata($post); ?>


        <div class="col-md-9">
            <h3><?php if ( get_field( 'gluten_free' ) ): ?><img src="../wp-content/themes/FRBBQ/dist/images/gluten-free.png" />
            <?php endif; // end of if field_name logic ?><?php the_title(); ?></h3>
            <h5><?php $brewer = get_the_term_list( $post->ID, 'brewer_taxonomy', '', ', ', ''); echo strip_tags($brewer); ?></h5>
            <h5><?php the_field ('type'); ?>: <?php the_field ('glass_size'); ?><?php the_field ('bottle_size'); ?><?php the_field ('can_size'); ?> <span class="lowercase"><?php the_field ('serving_type'); ?></span></h5>
        </div>
        <div class="col-md-3">
            <h3>$<?php the_field ('glass-price'); ?><?php the_field ('bottle_price'); ?><?php the_field ('can_price'); ?></h3>
            <h5>ABV: <?php the_field ('abv'); ?><br /> IBU: <?php the_field ('ibu'); ?></h5>
        </div>
        <div class="col-md-12">
            <?php the_content(); ?>
        </div>
    <hr class="col-md-12">


    <?php endforeach; ?>
</div>

<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>





<!--- *****************************
       Common Beers
****************************** --->

<?php $posts=get_field( 'common_beers'); ?>
<div class="row">
<h2><em>Other Beers</em></h2>

<?php if( $posts ): ?>
<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
<?php setup_postdata($post); ?>
    
    <div class="col-md-9">
        <h3><?php if ( get_field( 'gluten_free' ) ): ?><img src="../wp-content/themes/FRBBQ/dist/images/gluten-free.png" />
            <?php endif; // end of if field_name logic ?><?php the_title(); ?> | <?php $brewer = get_the_term_list( $post->ID, 'brewer_taxonomy', '', ', ', ''); echo strip_tags($brewer); ?></h3>
        <h5><?php the_field ('type'); ?>: <?php the_field ('glass_size'); ?><?php the_field ('bottle_size'); ?><?php the_field ('can_size'); ?> <span class="lowercase"><?php the_field ('serving_type'); ?></span></h5>
    </div>
    <div class="col-md-3">
        <h3>$<?php the_field ('glass-price'); ?><?php the_field ('bottle_price'); ?><?php the_field ('can_price'); ?></h3>
    </div>
    <?php endforeach; ?>

</div>

<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php endif; ?>

<div class="clearfix">
    <br />
    <br />
</div>