<div class="col-sm-6" style="margin-top: 30px !important; margin-right: 10px !important;">
    <div class="row border" style="height: 10.5in; display: table-cell">

        <img src="../wp-content/uploads/2016/02/logo.png" width="100%" />

        <!--- *****************************
                    Wines
       ****************************** --->
        <div class='drink-container'>
            <section>
                <h2 style="margin-top: 20px !important;">Wines</h2>
                <ul class="leaders">
                    <?php $posts=get_field( 'wine'); ?>
                    <?php if( $posts ): ?>
                    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                    <li class="drink">
                        <span><span style="color:#572314 !important;"><?php the_title(); ?></span> |
                        <?php the_field ( 'type'); ?> |
                        <?php the_field ( 'glass_size'); ?>
                        <?php the_field ( 'bottle_size'); ?>
                        <?php the_field ( 'can_size'); ?>
                        </span>
                        <span>$<?php the_field ('glass-price'); ?></span>
                    </li>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                    <?php endif; ?>
                </ul>
            </section>
            <hr />
            <section>
                <h2>Cocktails</h2>
                <ul class="leaders">
                    <?php $posts=get_field( 'specialty_cocktails'); ?>
                    <?php if( $posts ): ?>
                    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                    <li class="drink">
                        <span style="color:#572314 !important;"><?php the_title(); ?></span>
                        <span>$<?php the_field ('glass-price'); ?></span>
                        <div class="details">
                            <?php the_content(); ?>
                        </div>
                    </li>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                    <?php endif; ?>
                </ul>
            </section>
            <hr />
            <section>
                <h2>Bottled/Can Beers</h2>
                <ul class="leaders">
                    <?php $posts=get_field( 'common_beers'); ?>
                    <?php if( $posts ): ?>
                    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                    <li class="drink">
                        <span><span style="color:#572314 !important;"><?php the_title(); ?></span> |
                        <?php the_field ( 'type'); ?> |
                        <?php the_field ( 'glass_size'); ?>
                        <?php the_field ( 'bottle_size'); ?>
                        <?php the_field ( 'can_size'); ?>
                        </span>
                        <span>$<?php the_field ('bottle_price'); ?><?php the_field ('can_price'); ?></span>
                    </li>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                    <?php endif; ?>
                </ul>
            </section>
        </div>
    </div>
</div>


<div class="col-sm-6" style="margin-top: 30px !important; margin-left: 10px !important;">
        <div class="row border" style="height: 10.5in; display: table-cell">

        <img src="../wp-content/uploads/2016/02/logo.png" width="100%" />

        <!--- *****************************
                    Wines
       ****************************** --->
        <div class='drink-container'>
            <section>
                <h2 style="margin-top: 20px !important;">Wines</h2>
                <ul class="leaders">
                    <?php $posts=get_field( 'wine'); ?>
                    <?php if( $posts ): ?>
                    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                    <li class="drink">
                        <span><span style="color:#572314 !important;"><?php the_title(); ?></span> |
                        <?php the_field ( 'type'); ?>
                        </span>
                        <span>$<?php the_field ('glass-price'); ?></span>
                    </li>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                    <?php endif; ?>
                </ul>
            </section>
            <hr />
            <section>
                <h2>Cocktails</h2>
                <ul class="leaders">
                    <?php $posts=get_field( 'specialty_cocktails'); ?>
                    <?php if( $posts ): ?>
                    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                    <li class="drink">
                        <span style="color:#572314 !important;"><?php the_title(); ?></span>
                        <span>$<?php the_field ('glass-price'); ?></span>
                        <div class="details">
                            <?php the_content(); ?>
                        </div>
                    </li>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                    <?php endif; ?>
                </ul>
            </section>
            <hr />
            <section>
                <h2>Bottled/Can Beers</h2>
                <ul class="leaders">
                    <?php $posts=get_field( 'common_beers'); ?>
                    <?php if( $posts ): ?>
                    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                    <li class="drink">
                        <span><span style="color:#572314 !important;"><?php the_title(); ?></span> |
                        <?php the_field ( 'type'); ?> 
                        </span>
                        <span>$<?php the_field ('bottle_price'); ?><?php the_field ('can_price'); ?></span>
                    </li>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                    <?php endif; ?>
                </ul>
            </section>
        </div>
    </div>
</div>