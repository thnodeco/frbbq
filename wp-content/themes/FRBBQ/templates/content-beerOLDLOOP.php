<!-- Begin custom beer loop -->
  <?php
  //Retrieve custom taxonomy terms using get_terms and the custom post type.
    $categories = get_terms('beer_category');
   //Iterate through each term
    foreach ( $categories as $category ) :
    ?>
    <div class="clearfix"></div>
        <div class="row ">

            <?php
           //Setup the query to retrieve the posts that exist under each term
            $posts = get_posts(array(
              'post_type' => 'beer_list',
              'orderby' => 'menu_order',
              'order' =>  'ASC',
              'meta_key' => 'type',
              'taxonomy' => 'beer_category',
              'term'  => $category->slug,
              'nopaging' => true,
              ));
            // Here's the second, nested foreach loop that cycles through the posts associated with this category
            foreach($posts as $post) :
              setup_postdata($post); ////set up post data for use in the loop (enables the_title(), etc without specifying a post ID
            ?>
                <div class="col-md-5 col-md-offset-1">
                    <div class="col-md-9">
                        <h3><?php the_title(); ?></h3>
                        <h5><?php echo $category->name; ?><br /><?php the_field ('type'); ?>: <?php the_field ('glass_size'); ?><?php the_field ('bottle_size'); ?><?php the_field ('can_size'); ?></h5>
                    </div>
                    <div class="col-md-3">
                        <h3>$<?php the_field ('glass-price'); ?><?php the_field ('bottle-price'); ?><?php the_field ('can-price'); ?></h3>
                        <h5>ABV: <?php the_field ('abv'); ?><br /> IBU: <?php the_field ('ibu'); ?></h5>
                    </div>
                    <div class="col-md-12">
                        <?php the_content(); ?>
                    </div>
                </div>

            <?php endforeach; ?>

      </div><!-- .row -->

  <?php endforeach; //Easy Peasy ?>


<div class="full cta box-shadow">
    <div class="col-md-8 col-md-offset-1 cta-text">
        <?php the_field( 'cta_text', 8); ?>
    </div>
    <div class="col-md-1">
        <button type="button" class="btn blk-btn">
            <a href="<?php the_field('cta_link', 8); ?>">
                <?php the_field( 'cta_button_text', 8); ?>
            </a>
        </button>
    </div>
</div>


<?php wp_link_pages([ 'before'=> '
<nav class="page-nav">
    <p>' . __('Pages:', 'sage'), 'after' => '</p>
</nav>']); ?>
