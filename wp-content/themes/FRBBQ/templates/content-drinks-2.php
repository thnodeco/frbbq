<div class="col-sm-6" style="margin-top: 30px !important; margin-right: 10px !important;">
    <div class="row border" style="height: 10.5in; display: table-cell; line-height: 145% !important;">
        <!--- *****************************
                    Craft Beers
       ****************************** --->
        <div class='drink-container'>
            <section>
                <h2>Craft Beers</h2>
                <ul class="leaders">
                    <?php $page_id=8 ; $posts=get_field( 'craft_beers', $page_id); ?>
                    <?php if( $posts ): ?>
                    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                    <li class="drink">
                        <span><span style="color:#572314 !important;"><?php if ( get_field( 'gluten_free' ) ): ?><img src="../wp-content/themes/FRBBQ/dist/images/gluten-free.png" height="12px" width="12px"/>
            <?php endif; // end of if field_name logic ?><?php the_title(); ?></span> |
                        <?php the_field ( 'type'); ?> |
                        <?php the_field ( 'glass_size'); ?>
                        <?php the_field ( 'bottle_size'); ?>
                        <?php the_field ( 'can_size'); ?>
                        </span>
                        <span>

            <?php if ( get_field( 'bottle_price' ) == "0.00" ): ?>
            <?php elseif ( get_field('bottle_price') ): ?>
            	$<?php the_field('bottle_price'); ?>
            <?php endif; ?>

            <?php if ( get_field( 'can_price' ) == "0.00" ): ?>
            <?php elseif ( get_field('can_price') ): ?>
            	$<?php the_field('can_price'); ?>
            <?php endif; ?>

                          <br />ABV: <?php the_field ( 'abv' ); ?><?php if ( get_field( 'ibu' ) == "" ): ?><?php else: // field_name returned false ?><br />IBU:<?php the_field ( 'ibu' ); ?><?php endif; // end of if field_name logic ?></span>
                        <div class="details">
                            <?php the_content(); ?>
                        </div>
                    </li>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                    <?php endif; ?>
                </ul>
            </section>

        </div>
    </div>
</div>

<div class="col-sm-6" style="margin-top: 30px !important; margin-right: 10px !important;">
    <div class="row border" style="height: 10.5in; display: table-cell; line-height: 145% !important;">
        <!--- *****************************
                    Craft Beers
       ****************************** --->
        <div class='drink-container'>
            <section>
                <h2>Craft Beers</h2>
                <ul class="leaders">
                    <?php $page_id=8 ; $posts=get_field( 'craft_beers', $page_id); ?>
                    <?php if( $posts ): ?>
                    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                    <li class="drink">
                        <span><span style="color:#572314 !important;"><?php if ( get_field( 'gluten_free' ) ): ?><img src="../wp-content/themes/FRBBQ/dist/images/gluten-free.png" height="12px" width="12px"/>
            <?php endif; // end of if field_name logic ?><?php the_title(); ?></span> |
                        <?php the_field ( 'type'); ?> |
                        <?php the_field ( 'glass_size'); ?>
                        <?php the_field ( 'bottle_size'); ?>
                        <?php the_field ( 'can_size'); ?>
                        </span>
                        <span>
            <?php if ( get_field( 'bottle_price' ) == "0.00" ): ?>
            <?php elseif ( get_field('bottle_price') ): ?>
            	$<?php the_field('bottle_price'); ?>
            <?php endif; ?>

            <?php if ( get_field( 'can_price' ) == "0.00" ): ?>
            <?php elseif ( get_field('can_price') ): ?>
            	$<?php the_field('can_price'); ?>
            <?php endif; ?>

                          <br />ABV: <?php the_field ( 'abv' ); ?><?php if ( get_field( 'ibu' ) == "" ): ?><?php else: // field_name returned false ?><br />IBU:<?php the_field ( 'ibu' ); ?><?php endif; // end of if field_name logic ?></span>
                        <div class="details">
                            <?php the_content(); ?>
                        </div>
                    </li>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                    <?php endif; ?>
                </ul>
            </section>

        </div>
    </div>
</div>
