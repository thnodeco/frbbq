<?php $featured = new WP_Query(array(
  'post_type' => 'menu_items',
  'menu_category' => 'Featured',
  'post_per_page' => '1',
)); ?>

<div class="clearfix"></div>

          <div style="background-color: #1f0202; display: inline-block; float:right; width: 33%; padding: 20px 45px; margin-right:12rem; margin-top:-90px;">
          <?php while ($featured->have_posts()) : $featured->the_post(); ?>
            <h2><em><?php the_title(); ?> - $<?php the_field ('price'); ?></em></h2>
            <img src="http://placehold.it/470x270" style="margin-bottom:20px;"/>
            <span style="color:#fffeee"><?php the_content(); ?></span>
          </div>


  <?php endwhile; ?> <?php wp_reset_query(); ?>

<!-- Begin custom menu loop -->
  <?php
  //Retrieve custom taxonomy terms using get_terms and the custom post type.
    $categories = get_terms('menu_category');
   //Iterate through each term
    foreach ( $categories as $category ) :
    ?>
      <div class="row">

          <h3 class="col-md-offset-1"><em><?php echo $category->name; ?></em></h3>

            <?php
           //Setup the query to retrieve the posts that exist under each term
            $posts = get_posts(array(
              'post_type' => 'menu_items',
              'orderby' => 'menu_order',
              'order' =>  'ASC',
              'taxonomy' => $category->taxonomy,
              'term'  => $category->slug,
              'nopaging' => true,
              ));
            // Here's the second, nested foreach loop that cycles through the posts associated with this category
            foreach($posts as $post) :
              setup_postdata($post); ////set up post data for use in the loop (enables the_title(), etc without specifying a post ID
            ?>
                <div class="col-md-5 col-md-offset-1">
                    <div class="col-md-9">
                        <h3><?php the_title(); ?></h3>
                        <?php the_content(); ?>
                    </div>
                    <div class="col-md-3">
                        <h3>$<?php the_field ('price'); ?></h3>
                    </div>
                </div>

            <?php endforeach; ?>

      </div><!-- .row -->

  <?php endforeach; //Easy Peasy ?>

</div><!-- .row -->

<div class="full cta box-shadow">
    <div class="col-md-8 col-md-offset-1 cta-text">
        <?php the_field( 'cta_text', 4); ?>
    </div>
    <div class="col-md-1">
        <button type="button" class="btn blk-btn">
            <a href="<?php the_field('cta_link', 4); ?>">
                <?php the_field( 'cta_button_text', 4); ?>
            </a>
        </button>
    </div>
</div>


<?php wp_link_pages([ 'before'=> '
<nav class="page-nav">
    <p>' . __('Pages:', 'sage'), 'after' => '</p>
</nav>']); ?>
