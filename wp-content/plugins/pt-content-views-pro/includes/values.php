<?php

/**
 * Define values for input, select...
 *
 * @package   PT_Content_Views_Pro
 * @author    PT Guy <http://www.contentviewspro.com/>
 * @license   GPL-2.0+
 * @link      http://www.contentviewspro.com/
 * @copyright 2014 PT Guy
 */
if ( !class_exists( 'PT_CV_Values_Pro' ) ) {

	/**
	 * @name PT_CV_Values_Pro
	 * @todo Define values for input, select...
	 */
	class PT_CV_Values_Pro {
		/**
		 * Get Bootstrap styles for thumbnail
		 */
		static function field_thumbnail_styles() {
			// All available thumbnail sizes
			$result = array(
				'img-none'		 => __( 'No style', PT_CV_DOMAIN_PRO ),
				'img-rounded'	 => __( 'Round edge', PT_CV_DOMAIN_PRO ),
				'img-thumbnail'	 => __( 'Border', PT_CV_DOMAIN_PRO ),
				'img-circle'	 => __( 'Circle', PT_CV_DOMAIN_PRO ),
				'img-shadow'	 => __( 'Shadow', PT_CV_DOMAIN_PRO ),
			);

			return $result;
		}

		static function auto_thumbnail() {
			$result = array(
				'image'			 => __( 'Image', PT_CV_DOMAIN_PRO ),
				'video-audio'	 => __( 'Video / Audio', PT_CV_DOMAIN_PRO ),
				'none'			 => __( 'None', PT_CV_DOMAIN_PRO ),
			);

			return $result;
		}

		/**
		 * Style color for button
		 *
		 * @param array $text
		 *
		 * @return array
		 */
		static function field_style_options( $text, $unset = array() ) {

			$styles = array( 'primary', 'info', 'success', 'danger', 'default', 'warning', 'link' );

			$result = array();
			foreach ( $styles as $style ) {
				if ( !in_array( $style, (array) $unset ) ) {
					$result[ $style ] = PT_CV_Html::html_button( $style, $text, '', 'btn-sm' );
				}
			}

			return $result;
		}

		/**
		 * Return quick filter options for Woocommerce
		 */
		static function field_product_lists() {
			$result = array(
				'sale_products'			 => __( 'Sale products', PT_CV_DOMAIN_PRO ),
				'recent_products'		 => __( 'Recent products', PT_CV_DOMAIN_PRO ),
				'best_selling_products'	 => __( 'Best selling products', PT_CV_DOMAIN_PRO ),
				'featured_products'		 => __( 'Featured products', PT_CV_DOMAIN_PRO ),
				'top_rated_products'	 => __( 'Top rated products', PT_CV_DOMAIN_PRO ),
				''						 => __( 'None of above (use other settings below)', PT_CV_DOMAIN_PRO ),
			);

			return $result;
		}

		/**
		 * Pro View types
		 *
		 * @return array
		 */
		static function view_type_pro() {
			$result = array(
				'pinterest'	 => __( 'Pinterest', PT_CV_DOMAIN_PRO ),
				'masonry'	 => __( 'Masonry', PT_CV_DOMAIN_PRO ),
				'timeline'	 => __( 'Timeline', PT_CV_DOMAIN_PRO ),
				'glossary'	 => __( 'Glossary', PT_CV_DOMAIN_PRO ),
				'one_others' => __( 'One and others', PT_CV_DOMAIN_PRO ),
			);

			return $result;
		}

		/**
		 * Pagination alignment options
		 *
		 * @return array
		 */
		static function pagination_alignment() {

			$result = array(
				'left'	 => __( 'Left', PT_CV_DOMAIN_PRO ),
				'center' => __( 'Center', PT_CV_DOMAIN_PRO ),
				'right'	 => __( 'Right', PT_CV_DOMAIN_PRO ),
			);

			$result = apply_filters( PT_CV_PREFIX_ . 'pagination_alignment', $result );

			return $result;
		}

		/**
		 * Font families
		 *
		 * @return array
		 */
		static function font_families() {
			$fonts_data		 = PT_CV_Functions_Pro::get_google_fonts();
			$font_families	 = array_keys( $fonts_data );

			$result			 = array();
			$result[ '' ]	 = __( '- Default font -', PT_CV_DOMAIN_PRO );

			foreach ( $font_families as $font ) {
				$result[ $font ] = $font;
			}

			return $result;
		}

		/**
		 * Font styles
		 *
		 * @return array
		 */
		static function font_styles() {
			$styles = array( '100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic' );

			$result			 = array();
			$result[ '' ]	 = __( '- Default weight, style -', PT_CV_DOMAIN_PRO );

			foreach ( $styles as $style ) {
				$result[ $style ] = ($style === 'regular') ? 'normal' : $style;
			}

			return $result;
		}

		static function font_decoration() {
			$styles = array(
				'none'		 => __( 'None', PT_CV_DOMAIN_PRO ),
				'underline'	 => __( 'Underline', PT_CV_DOMAIN_PRO ),
			);

			$result			 = array();
			$result[ '' ]	 = __( '- Default decoration -', PT_CV_DOMAIN_PRO );

			foreach ( $styles as $style ) {
				$result[ $style ] = $style;
			}

			return $result;
		}

		/**
		 * Array of a - z characters
		 */
		static function array_a_z() {
			$characters = range( 'a', 'z' );

			$result = array_combine( $characters, $characters );

			return array_merge( array( __( 'Select character', PT_CV_DOMAIN_PRO ) ), $result );
		}

		/**
		 * Text direction
		 */
		static function text_direction() {
			$result = array(
				'ltr'	 => __( 'Left to Right', PT_CV_DOMAIN_PRO ),
				'rtl'	 => __( 'Right to Left', PT_CV_DOMAIN_PRO ),
			);

			return $result;
		}

		/**
		 * Taxonomy filter style
		 */
		static function taxonomy_filter_style( $class = 'filter-bar' ) {
			$items	 = array( 'Lorem', 'Taxo' );
			$class	 = PT_CV_PREFIX . $class;

			$result = array(
				'btn-group'			 => PT_CV_Html_Pro::filter_html_btn_group( $class, $items ),
				'vertical-dropdown'	 => PT_CV_Html_Pro::filter_html_vertical_dropdown( $class, $items ),
				'breadcrumb'		 => PT_CV_Html_Pro::filter_html_breadcrumb( $class, $items ),
				'group_by_taxonomy'	 => __( 'Group options by Taxonomy (recommended when multiple taxonomies are selected)', PT_CV_DOMAIN_PRO ),
			);

			return $result;
		}

		/**
		 * Taxonomy filter position
		 */
		static function taxonomy_filter_position() {
			$result = array(
				'left'	 => __( 'Left', PT_CV_DOMAIN_PRO ),
				'center' => __( 'Center', PT_CV_DOMAIN_PRO ),
				'right'	 => __( 'Right', PT_CV_DOMAIN_PRO ),
			);

			return $result;
		}

		/**
		 * List of custom fields
		 */
		static function custom_fields( $include_empty = false ) {
			global $wpdb;

			$keys = $wpdb->get_col(
				"SELECT meta_key
				FROM $wpdb->postmeta
				GROUP BY meta_key
				ORDER BY meta_key"
			);
			if ( $keys ) {
				natcasesort( $keys );
			}

			// Final result
			$result = $include_empty ? array( '' => __( '- Select -', PT_CV_DOMAIN_PRO ) ) : array();
			foreach ( $keys as $key ) {
				/**
				 * Don't hide protected meta fields, to able to select data of The Events Calendar...
				 * @since 1.6.5
				 *
				  if ( is_protected_meta( $key, 'post' ) ) {
				  continue;
				  }
				 *
				 */
				$result[ esc_attr( $key ) ] = esc_html( $key );
			}

			// Sort values of param by saved order
			$result = apply_filters( PT_CV_PREFIX_ . 'settings_sort_single', $result, 'custom-fields-list' );

			return $result;
		}

		/**
		 * Post date options
		 */
		static function post_date() {
			$result = array(
				'today'			 => __( 'Today', PT_CV_DOMAIN_PRO ),
				'custom_date'	 => __( 'Custom date', PT_CV_DOMAIN_PRO ),
				'from_today'	 => __( 'Today and future', PT_CV_DOMAIN_PRO ),
				'custom_time'	 => __( 'Custom time (from &rarr; to)', PT_CV_DOMAIN_PRO ),
				'yesterday'		 => __( 'Yesterday', PT_CV_DOMAIN_PRO ),
				'week_ago'		 => __( '1 week ago (to today)', PT_CV_DOMAIN_PRO ),
				'this_week'		 => __( 'This week', PT_CV_DOMAIN_PRO ),
				'month_ago'		 => __( '1 month ago (to today)', PT_CV_DOMAIN_PRO ),
				'this_month'	 => __( 'This month', PT_CV_DOMAIN_PRO ),
				'year_ago'		 => __( '1 year ago (to today)', PT_CV_DOMAIN_PRO ),
				'this_year'		 => __( 'This year', PT_CV_DOMAIN_PRO ),
			);

			return $result;
		}

		/**
		 * Post align options
		 */
		static function text_align() {
			$result = array(
				'left'		 => __( 'Left', PT_CV_DOMAIN_PRO ),
				'right'		 => __( 'Right', PT_CV_DOMAIN_PRO ),
				'center'	 => __( 'Center', PT_CV_DOMAIN_PRO ),
				'justify'	 => __( 'Justify', PT_CV_DOMAIN_PRO ),
			);

			return $result;
		}

		/**
		 * Show what from parent page
		 */
		static function parent_page_options() {
			$result = array(
				''			 => __( '- Select -', PT_CV_DOMAIN_PRO ),
				'children'	 => __( 'Show children of current page', PT_CV_DOMAIN_PRO ),
				'siblings'	 => __( 'Show siblings of current page', PT_CV_DOMAIN_PRO ),
				'all'		 => __( 'Show children & siblings of current page', PT_CV_DOMAIN_PRO ),
			);

			return $result;
		}

		/**
		 * Show what from parent page
		 */
		static function parent_page_info() {
			$result = array(
				'title'		 => __( 'Title', PT_CV_DOMAIN_PRO ),
				'title_link' => __( 'Title & Link', PT_CV_DOMAIN_PRO ),
				''			 => __( '- Nothing -', PT_CV_DOMAIN_PRO ),
			);

			return $result;
		}

		/**
		 * Custom field types
		 */
		static function custom_field_type() {
			$result = array(
				'CHAR'		 => __( 'Text', PT_CV_DOMAIN_PRO ),
				'NUMERIC'	 => __( 'Number', PT_CV_DOMAIN_PRO ),
				'DATE'		 => __( 'Date', PT_CV_DOMAIN_PRO ),
				'BINARY'	 => __( 'True/False', PT_CV_DOMAIN_PRO ),
			);

			return $result;
		}

		/**
		 * Setting options for Sticky posts
		 */
		static function sticky_posts() {
			$result = array(
				'exclude'		 => __( 'Exclude from output', PT_CV_DOMAIN_PRO ),
				'default'		 => __( 'Show in natural position', PT_CV_DOMAIN_PRO ),
				'prepend-all'	 => __( 'Place all sticky posts at the top of list', PT_CV_DOMAIN_PRO ),
				'prepend'		 => __( 'Place current sticky posts (which match all settings) at the top of list', PT_CV_DOMAIN_PRO ),
			);

			return $result;
		}

		/**
		 * List of social buttons
		 */
		static function social_buttons() {
			$result = array(
				'facebook'	 => __( 'Facebook', PT_CV_DOMAIN_PRO ),
				'twitter'	 => __( 'Twitter', PT_CV_DOMAIN_PRO ),
				'googleplus' => __( 'Google Plus', PT_CV_DOMAIN_PRO ),
				'linkedin'	 => __( 'Linkedin', PT_CV_DOMAIN_PRO ),
				'pinterest'	 => __( 'Pinterest', PT_CV_DOMAIN_PRO ),
			);

			$result = apply_filters( PT_CV_PREFIX_ . 'social_buttons', $result );

			return $result;
		}

		/**
		 * Animation effects for content
		 * @return type
		 */
		static function content_animation() {
			$result = array(
				''			 => __( 'Fade in', PT_CV_DOMAIN_PRO ),
				'effect-lr'	 => __( 'Slide left right', PT_CV_DOMAIN_PRO ),
				'effect-ud'	 => __( 'Slide up down', PT_CV_DOMAIN_PRO ),
			);

			$result = apply_filters( PT_CV_PREFIX_ . 'content_animation', $result );

			return $result;
		}

		static function term_filter_custom() {
			$result = array(
				''			 => __( '- None -', PT_CV_DOMAIN_PRO ),
				'as_output'	 => __( 'Show terms as output', PT_CV_DOMAIN_PRO ),
				'as_heading' => __( 'Show (first) term as heading of output', PT_CV_DOMAIN_PRO ),
			);

			$result = apply_filters( PT_CV_PREFIX_ . 'term_filter_custom', $result );

			return $result;
		}

		/**
		 * View format of Layout: One and others
		 * @return type
		 */
		static function view_format_one_and_others() {
			$label	 = __( 'One post %s other posts', PT_CV_DOMAIN_PRO );
			$icon	 = '<code><span class="dashicons dashicons-arrow-%s-alt" style="margin-top: 4px;"></span>%s</code>';
			$result	 = array(
				'2'	 => sprintf( $label, sprintf( $icon, 'left', __( 'on left of', PT_CV_DOMAIN_PRO ) ) ),
				'1'	 => sprintf( $label, sprintf( $icon, 'up', __( 'above of', PT_CV_DOMAIN_PRO ) ) ),
			);

			return $result;
		}

		static function width_prop_one_and_others() {
			$result = array(
				'6-6'	 => '1 : 1',
				'8-4'	 => '2 : 1',
				'4-8'	 => '1 : 2',
			);

			return $result;
		}

		/**
		 * Fields to display of other posts
		 * @return type
		 */
		static function one_others_fields() {
			$result = array(
				'thumbnail'		 => __( 'Thumbnail', PT_CV_DOMAIN_PRO ),
				'title'			 => __( 'Title', PT_CV_DOMAIN_PRO ),
				'meta-fields'	 => __( 'Date', PT_CV_DOMAIN_PRO ),
				'content'		 => __( 'Excerpt', PT_CV_DOMAIN_PRO ),
				'readmore'		 => __( 'Read more', PT_CV_DOMAIN_PRO ),
			);

			return $result;
		}

		/**
		 * Option to display taxonomy
		 * @return type
		 */
		static function meta_field_taxonomy_display_what() {
			$result = array(
				''				 => __( 'All terms of post', PT_CV_DOMAIN_PRO ),
				'custom_taxo'	 => __( 'Let me choose', PT_CV_DOMAIN_PRO ),
			);

			return $result;
		}

		static function meta_field_author_settings() {
			$result = array(
				''				 => __( 'Show name', PT_CV_DOMAIN_PRO ),
				'author_avatar'	 => __( 'Show avatar', PT_CV_DOMAIN_PRO ),
			);

			return $result;
		}

	}

}