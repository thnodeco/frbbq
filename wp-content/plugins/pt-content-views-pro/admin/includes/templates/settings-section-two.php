<style>
	.wrap {
		background: url(<?php echo esc_url( plugins_url( 'admin/assets/images/gold-medal.png', PT_CV_FILE_PRO ) ); ?>) right top no-repeat;
	}
	.wrap h3 {
		font-size: 20px;
	}
	input[type="text"] {
		width: 600px;
	}
	.form-table th, .form-table td {
		padding-bottom: 0px;
		padding-top: 10px;
	}
	#heading-setting-account {
		margin: 30px 0 -10px 0;
	}
	textarea {
		max-height:300px;
		width:49%;
		overflow:auto;
		background-color:#FFFFDD;
		resize:vertical;
	}
	#custom_js {margin-left: 1%;}
</style>

<h3>&DoubleRightArrow; <a style="font-size: 18px;" href="http://www.contentviewspro.com/docs/?utm_source=settings_page" target="_blank">Content Views Documentation</a></h3>

<hr>

<p>Thank you so much for using Content Views Pro. <a href="http://www.contentviewspro.com/contact/">Contact us!</a></p>