/**
 * Script Name: Collapsible List
 *
 * @package   PT_Content_Views_Pro
 * @author    PT Guy (http://www.contentviewspro.com/)
 * @license   GPL-2.0+
 * @link      http://www.contentviewspro.com/
 * @copyright 2014 PT Guy
 */

( function ( $ ) {
	"use strict";

	$.PT_CV_Collapsible = $.PT_CV_Collapsible || { };

	PT_CV_PUBLIC = PT_CV_PUBLIC || { };

	$.PT_CV_Collapsible = function ( options ) {
		this.options = options;

		this._toggle_panel( this.options.collapse_box );
	};

	$.PT_CV_Collapsible.prototype = {
		_toggle_panel: function ( $selector ) {
			var _prefix = this.options._prefix;

			// On show action
			$( $selector + ' .panel-collapse' ).on( 'shown.bs.collapse', function () {
				$( this ).prev().find( ".glyphicon" ).removeClass( "glyphicon-plus" ).addClass( "glyphicon-minus" );
			} );

			// On hide action
			$( $selector + ' .panel-collapse' ).on( 'hidden.bs.collapse', function () {
				$( this ).prev().find( ".glyphicon" ).removeClass( "glyphicon-minus" ).addClass( "glyphicon-plus" );
			} );

			var $collapse_group = $( $selector ).find( '.panel-group' ).first();

			// Trigger to show the first item
			if ( $collapse_group.attr( 'data-first-open' ) === 'yes' ) {
				var $collapse_box = $( $selector ).find( '.' + _prefix + 'page' ).length ? $( $selector ).find( '.' + _prefix + 'page:visible' ) : $( $selector );

				setTimeout( function () {
					$collapse_box.find( '.panel-collapse' ).first().collapse( 'show' );
				}, 500 );
			}

			// Multiple-open
			if ( $collapse_group.attr( 'data-multiple-open' ) === 'yes' ) {
				$( $selector + ' .panel-heading' + ', ' + $selector + ' .panel-heading a' ).on( 'click', function () {
					var $heading = $( this ).is( 'a' ) ? $( this ).closest( '.panel-heading' ) : $( this );
					$heading.next().collapse( 'toggle' );
				} );
			}

			// Handle toogle when click on +, - button
			$( 'span.panel-collapsed', $selector ).on( 'click', function () {
				$( this ).parent().next().collapse( 'toggle' );
			} );
		}
	};

	$( function () {
		var _prefix = PT_CV_PUBLIC._prefix;

		new $.PT_CV_Collapsible( { _prefix: _prefix, collapse_box: '.' + _prefix + 'collapsible' } );
	} );
}( jQuery ) );