<?php
if ( class_exists( 'Tribe__Events__Facebook__API' ) ) {
	return;
}

class Tribe__Events__Facebook__API {
	protected static $instance;
	public $last_response = null;

	/**
	* inforce singleton factory method
	*
	* @return Tribe__Events__Facebook_API
	*/
	public static function instance() {
		if ( ! isset( self::$instance ) ) {
			$className = __CLASS__;
			self::$instance = new $className;
		}

		return self::$instance;
	}

	/**
	 * retrive the data object of a json response
	 *
	 * @since 1.0
	 * @author jkudish
	 *
	 * @param string $url the URL to retrieve
	 *
	 * @return mixed the result of the data object
	 */
	public function retrieve_data_from( $url ) {
		$json = $this->json_retrieve( $url );
		return $json->data;
	}


	/**
	 * retrieve the body of a page using the HTTP API and json decode the result
	 *
	 * @since 1.0
	 * @author jkudish
	 *
	 * @param string $url the URL to retrieve
	 *
	 * @return string the json string
	 */
	public function json_retrieve( $url ) {
		add_filter( 'http_request_timeout', array( $this, 'http_request_timeout' ) );
		$response = wp_remote_get( $url );

		// keep this for testing or inspection of failure
		$this->last_response = $response;

		$response = json_decode( wp_remote_retrieve_body( $response ) );
		remove_filter( 'http_request_timeout', array( $this, 'http_request_timeout' ) );
		return $response;
	}

	/**
	 * increase the HTTP request timeout because sometimes FB is slow
	 *
	 * @since 1.0
	 * @author jkudish
	 *
	 * @param int $timeout the original timeout
	 *
	 * @return int the filtered timeout
	 */
	public function http_request_timeout( $timeout ) {
		return (int) apply_filters( 'tribe_fb_http_request_timeout', 10, $timeout );
	}

}

