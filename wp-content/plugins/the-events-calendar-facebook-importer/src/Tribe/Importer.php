<?php

/**
 * @see https://developers.facebook.com/docs/reference/api/event/ Facebook Event Documentation
 */
class Tribe__Events__Facebook__Importer {

	const VERSION = '4.1';

	const REQUIRED_TEC_VERSION = '4.1';

	protected static $instance;

	public static $plugin_root;

	public static $fb_graph_url = 'https://graph.facebook.com/v2.4/';

	public static $default_app_id = '404935409546807';

	public static $default_app_secret = 'a1300fded85f77fc64f14fdb69395545';

	public static $origin = 'facebook-importer';

	public static $valid_reccurence_patterns = array(
		'weekly' => 604800,
		'daily' => 86400,
		'twicedaily' => 43200,
		'hourly' => 3600,
	);

	public $errors = array();

	public $errors_images = array();

	public $success = false;

	public $imported_total = 0;

	public $empty_key_notice = false;

	/** @var array representing the last response from a JSON request */
	public $last_response = null;

	/** @var Tribe__Events__Facebook__Resync */
	protected $resync_tool;

	/**
	 * Object representing a Facebook entity.
	 *
	 * @var stdClass
	 */
	protected $fb_object;

	/**
	 * Date format used during import of events.
	 *
	 * @var string
	 */
	protected $date_format = '';


	/**
	 * class constructor
	 * run necessary hooks
	 *
	 * @since 1.0
	 * @author jkudish
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'init' ) );
		add_action( 'admin_init', array( $this, 'reset_default_app' ) );
		add_action( 'admin_init', array( $this, 'resync_tool' ) );
		add_filter( 'tribe_addons_tab_fields', array( $this, 'add_addon_fields' ) );
		add_action( 'tribe_fb_auto_import', array( $this, 'do_auto_import' ) );
		add_action( 'tribe_import_options_updated', array( $this, 'manage_import_schedule' ) );
		add_filter( 'cron_schedules', array( $this, 'cron_add_weekly' ) );
		add_action( 'wp_before_admin_bar_render', array( $this, 'addFacebookToolbarItems' ), 30 );
		add_action( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'addLinksToPluginActions' ) );
		add_action( 'before_delete_post', array( $this, 'setDeletedEventArrayOption' ), 10, 1 );
		add_filter( 'tribe_help_tab_forums_url', array( $this, 'helpTabForumsLink' ), 100 );
		add_filter( 'tribe_fb_get_app_id', array( $this, 'check_empty_app_keys' ) );
		add_filter( 'tribe_fb_get_app_secret', array( $this, 'check_empty_app_keys' ) );

		add_filter( 'tribe_import_available_options', array( $this, 'add_import_available_fields' ), 15 );
		add_filter( 'tribe_import_tabs', array( $this, 'add_import_tab' ) );
		add_action( 'tribe_import_render_tab_facebook', array( $this, 'do_import_page' ) );
		add_action( 'tribe_import_general_settings', array( $this, 'add_settings_fields' ), 20 );

		$this->date_format = apply_filters( 'tribe_fb_date_format', get_option( 'date_format' ) );
	}

	/**
	 * run hooks on WP init
	 *
	 * @since 1.0
	 * @author jkudish
	 * @return void
	 */
	public function init() {
		load_plugin_textdomain( 'tribe-fb-import', false, dirname( dirname( dirname( plugin_basename( __FILE__ ) ) ) ) . '/lang/' );
	}

	/**
	 * get the url used for the FB graph API
	 *
	 * @since 1.0
	 * @author jkudish
	 * @return string the graph API url
	 */
	public function get_graph_url() {
		return apply_filters( 'tribe_fb_get_graph_url', self::$fb_graph_url );
	}


	public function add_import_tab( $tabs ) {
		$tabs[ __( 'Facebook', 'tribe-fb-import' ) ] = 'facebook';

		return $tabs;
	}

	public function add_import_available_fields( $fields = array() ) {
		$fields[] = 'fb_uids';
		$fields[] = 'fb_enable_GoogleMaps';
		$fields[] = 'fb_auto_import';
		$fields[] = 'fb_auto_frequency';
		return $fields;
	}

	public function add_settings_fields( $fields = array() ) {
		$fb_import_tab = Tribe__Events__Importer__Admin_Page::instance()->get_url( array( 'tab' => 'facebook' ) );

		$newfields = array(
			'fb-title' => array(
				'type' => 'html',
				'html' => '<h3>' . esc_html__( 'Facebook Import Settings', 'tribe-fb-import' ) . '</h3>',
			),
			'fb-import-instructions' => array(
				'type' => 'html',
				'html' =>
					'<p>' . esc_html__( 'You can retrieve and import events belonging to a Facebook organization or a Facebook page. You will need the username(s) or ID of each organization or page that you want to fetch events from. We do not currently support retrieving events from personal profiles. If you want to import an event from an individual, you can do that with the event ID on the', 'tribe-fb-import' ) . ' <a href="' . $fb_import_tab . '">' . esc_html__( 'Import: Facebook page', 'tribe-fb-import' ) . '</a></p>
					<p>' . sprintf( esc_html__( "A page or organization's username or ID can be found in the URL used to access its profile. Modern Tribe's page is %s and the username is 'ModernTribeInc'. If a page or organization doesn't have a username, you will see the ID (numerical) in the URL.", 'tribe-fb-import' ), '<a href="https://www.facebook.com/ModernTribeInc" target="_blank">https://www.facebook.com/ModernTribeInc</a>' ) . '</p>',
			),
			'fb-form-content-start' => array(
				'type' => 'html',
				'html' => '<div class="tribe-settings-form-wrap">',
			),
			'fb_uids' => array(
				'type' => 'textarea',
				'label' => __( 'Organization and page usernames / IDs to fetch events from', 'tribe-fb-import' ),
				'tooltip' => __( 'Please put one entry per line.', 'tribe-fb-import' ) . '<br>' . __( 'Follow the instructions above to find usernames or IDs.', 'tribe-fb-import' ) . '<br />' . __( 'Events can only be fetched from organizations and pages, not individuals.', 'tribe-fb-import' ),
				'size' => 'medium',
				'validation_type' => 'alpha_numeric_multi_line_with_dots_and_dashes',
				'can_be_empty' => true,
				'parent_option' => Tribe__Events__Main::OPTIONNAME,
				'esc_display' => 'tribe_multi_line_remove_empty_lines',
			),
			'imported_post_status[facebook]' => array(
				'type' => 'dropdown',
				'label' => __( 'Default status to use for imported events', 'tribe-fb-import' ),
				'options' => Tribe__Events__Importer__Options::get_possible_stati(),
				'validation_type' => 'options',
				'parent_option' => Tribe__Events__Main::OPTIONNAME,
			),
			'fb_enable_GoogleMaps' => array(
				'type' => 'checkbox_bool',
				'label' => __( 'Enable Google Maps on imported events', 'tribe-fb-import' ),
				'tooltip' => __( 'Check to enable maps for imported events in the frontend. Please enable Google Maps on the "General" tab to ensure your events will have map options.', 'tribe-fb-import' ),
				'default' => true,
				'class' => 'google-embed-size',
				'validation_type' => 'boolean',
			),
			'fb_auto_import' => array(
				'type' => 'checkbox_bool',
				'label' => __( 'Auto import from Facebook', 'tribe-fb-import' ),
				'tooltip' => __( 'If selected, events will be automatically imported from Facebook at the set interval.', 'tribe-fb-import' ),
				'validation_type' => 'boolean',
				'parent_option' => Tribe__Events__Main::OPTIONNAME,
			),
			'fb_auto_frequency' => array(
				'type' => 'dropdown',
				'label' => __( 'Import frequency', 'tribe-fb-import' ),
				'tooltip' => __( 'How often should we fetch events from Facebook. Only applies if Auto Import is set.', 'tribe-fb-import' ),
				'options' => array( 'weekly' => __( 'Weekly', 'tribe-fb-import' ), 'daily' => __( 'Daily', 'tribe-fb-import' ), 'twicedaily' => __( 'Twice daily', 'tribe-fb-import' ), 'hourly' => __( 'Hourly', 'tribe-fb-import' ) ),
				'validation_type' => 'options',
				'parent_option' => Tribe__Events__Main::OPTIONNAME,
			),
			'fb-next' => array(
				'type' => 'html',
				'html' => $this->get_next_import_html(),
			),
			'fb-previous' => array(
				'type' => 'html',
				'html' => $this->get_previous_import_html(),
			),
			'fb-form-content-end' => array(
				'type' => 'html',
				'html' => '</div>',
			),
		);
		return array_merge( $fields, $newfields );
	}

	public function get_next_import_html() {
		$html = array();

		$timestamp = wp_next_scheduled( 'tribe_fb_auto_import' );

		if ( empty( $timestamp ) || false === $timestamp ) {
			return false;
		}

		$date = date( 'c', $timestamp );
		$human = sprintf(
			_x( 'in about %s', '%s = human-readable time difference', 'tribe-fb-import' ),
			human_time_diff( $timestamp, current_time( 'timestamp' ) )
		);

		// Start to Grab HTML
		ob_start();
		?>
		<fieldset id="tribe-field-fb-next" class="tribe-field tribe-field-dropdown tribe-size-medium">
			<legend class="tribe-field-label"><?php esc_html_e( 'Next Import', 'tribe-fb-import' ) ; ?></legend>
			<div class="tribe-field-wrap">
				<b title="<?php echo esc_attr( $date ); ?>"><?php echo esc_html( $human ); ?></b>
			</div>
		</fieldset>
		<div class="clear"></div>
		<?php

		// Fetch the HTML
		$html = ob_get_clean();

		return $html;
	}

	public function get_previous_import_html() {
		$html = array();

		$previous = get_transient( 'tribe_facebook_last_auto_import' );
		$is_estimated = false;

		if ( empty( $previous ) || false === $previous ) {
			$next = wp_next_scheduled( 'tribe_fb_auto_import' ) ;
			if ( empty( $next ) || false === $next ) {
				return false;
			}

			$previous = $next - WEEK_IN_SECONDS;
			$is_estimated = true;
		}

		$date = date( 'c', $previous );
		$human = sprintf(
			_x( 'about %s ago', '%s = human-readable time difference', 'tribe-fb-import' ),
			human_time_diff( $previous, current_time( 'timestamp' ) )
		);

		// Start to Grab HTML
		ob_start();
		?>
		<fieldset id="tribe-field-fb-previous" class="tribe-field tribe-field-dropdown tribe-size-medium">
			<legend class="tribe-field-label"><?php esc_html_e( 'Previous Import', 'tribe-fb-import' ); ?></legend>
			<div class="tribe-field-wrap">
				<b title="<?php echo esc_attr( $date ); ?>"><?php echo esc_html( $human ); ?></b>
			<?php if ( $is_estimated ) { ?>
				<p><small><?php esc_html_e( 'We couldn\'t find when the last auto-import happened, so the date above is an estimate.', 'tribe-fb-import' ); ?></small></p>
			<?php } ?>
			</div>
		</fieldset>
		<div class="clear"></div>
		<?php

		// Fetch the HTML
		$html = ob_get_clean();

		return $html;
	}

	/**
	 * If in admin check that app id & secret are not default,
	 * remove and notify if they are.
	 *
	 * @since  1.0.6
	 * @author tim@imaginesimplicity.com
	 * @return bool
	 */
	public function reset_default_app() {
		if ( ! is_admin() ) {
			return false;
		}
		$reset_default = false;
		if ( $this->get_app_id() == self::$default_app_id ){
			$reset_default = true;
			Tribe__Settings_Manager::set_option( 'fb_api_key', '' );
		}
		if ( $this->get_app_secret() == self::$default_app_secret ){
			$reset_default = true;
			Tribe__Settings_Manager::set_option( 'fb_api_secret', '' );
		}
		if ( $reset_default ){
			add_action( 'admin_notices', array( $this, 'default_app_notices' ) );
			return true;
		}
		return false;
	}

	/**
	 * Add the Facebook Fields to the Add-ons page on the correct position
	 *
	 * @param array $fields The array of existing fields added to the addons page
	 * @return array
	 */
	public function add_addon_fields( $fields = array() ) {
		$new_fields = array(
			'fb-start' => array(
				'type' => 'html',
				'html' => '<h3>' . esc_html__( 'Facebook', 'the-events-calendar' ) . '</h3>',
			),

			'fb-info-box' => array(
				'type' => 'html',
				'html' => '<p>' . esc_html__( 'You need a Facebook App ID and App Secret to access data via the Facebook Graph API to import your events from Facebook.', 'tribe-fb-import' ) . '</p>',
			),

			'fb_api_key' => array(
				'type' => 'text',
				'label' => esc_html__( 'Facebook App ID', 'the-events-calendar' ),
				'tooltip' => sprintf( __( '<p>%s to view or create your Facebook Apps', 'the-events-calendar' ), '<a href="https://developers.facebook.com/apps" target="_blank"></p>' . __( 'Click here', 'the-events-calendar' ) . '</a>' ),
				'size' => 'medium',
				'validation_type' => 'alpha_numeric',
				'can_be_empty' => true,
				'parent_option' => Tribe__Events__Main::OPTIONNAME,
			),
			'fb_api_secret' => array(
				'type' => 'text',
				'label' => esc_html__( 'Facebook App secret', 'the-events-calendar' ),
				'tooltip' => sprintf( __( '<p>%s to view or create your App Secret', 'the-events-calendar' ), '<a href="https://developers.facebook.com/apps" target="_blank"></p>' . __( 'Click here', 'the-events-calendar' ) . '</a>' ),
				'size' => 'medium',
				'validation_type' => 'alpha_numeric',
				'can_be_empty' => true,
				'parent_option' => Tribe__Events__Main::OPTIONNAME,
			),
		);

		return array_merge( (array) $fields, $new_fields );
	}

	/**
	 * @return Tribe__Events__Facebook__Resync
	 */
	public function resync_tool() {
		if ( empty( $this->resync_tool ) ) {
			$this->resync_tool = new Tribe__Events__Facebook__Resync;
		}

		return $this->resync_tool;
	}

	/**
	 * Notice for removing default app strings
	 *
	 * @since 1.0.6
	 * @author Reid
	 * @return void
	 */
	public function default_app_notices() {
		$tribe_facebook_settings_url = Tribe__Events__Importer__Admin_Page::instance()->get_url( array( 'tab' => 'facebook' ) );
		echo '<div class="error"><p>';
		printf( __( 'As of version %s, you need to enter your own Facebook App ID and App secret. Visit %s to generate yours. Enter your App ID and App Secret on the <a href=\'%s\'>event\'s settings page</a>. ', 'tribe-fb-import' ), '<a href="https://developers.facebook.com/apps">https://developers.facebook.com/apps</a>', self::VERSION, $tribe_facebook_settings_url );
		echo '</p></div>';
	}

	/**
	 * check if Facebook app keys are empty and notify on the admin
	 *
	 * @since  1.0.6
	 * @author tim@imaginesimplicity.com
	 *
	 * @param  string $key
	 *
	 * @return string $key
	 */
	public function check_empty_app_keys( $key ) {
		if ( empty( $key ) && ! $this->empty_key_notice ){
			add_action( 'admin_notices', array( $this, 'empty_app_notices' ) );
			$this->empty_key_notice = true;
		}
		return $key;
	}

	/**
	 * Translation Strings for App & Secret requirement
	 *
	 * @since 1.0.6
	 * @author Reid
	 * @return void
	 */
	public function empty_app_notices() {
		$developer_apps_link = '<a href="https://developers.facebook.com/apps">https://developers.facebook.com/apps</a>';
		$addons_apis_url = Tribe__Settings::instance()->get_url( array( 'tab' => 'addons' ) );
		$addons_apis_link = '<a href="' . $addons_apis_url . '">' . __( 'the Add-Ons APIs settings page', 'tribe-fb-import' ) . '</a>';

		echo '<div class="updated notice"><p>';

		printf( esc_html__( 'Signing up for a Facebook App ID and Secret only takes a second. Visit %s to create yours. ', 'tribe-fb-import' ),
			$developer_apps_link
		);

		printf( esc_html__( 'Visit %s to learn more about Facebook App Ids and Secrets. ', 'tribe-fb-import' ),
			'<a href="https://developers.facebook.com/docs/guides/canvas/">https://developers.facebook.com/docs/guides/canvas/</a>' );

		printf( esc_html__( 'You must enter a Facebook App ID and App Secret to continue importing events from Facebook. Visit %1$s to create yours then head over to %2$s to apply them.', 'tribe-fb-import' ),
			$developer_apps_link, $addons_apis_link
		);

		echo '</p></div>';
	}

	/**
	 * get the APP ID stored in the database or the default
	 *
	 * @since 1.0
	 * @author jkudish
	 * @return string the APP ID
	 */
	public function get_app_id() {
		return apply_filters( 'tribe_fb_get_app_id', Tribe__Settings_Manager::get_option( 'fb_api_key' ) );
	}

	/**
	 * get the APP secret stored in the database or the default
	 *
	 * @since 1.0
	 * @author jkudish
	 * @return string the APP secret
	 */
	public function get_app_secret() {
		return apply_filters( 'tribe_fb_get_app_secret', Tribe__Settings_Manager::get_option( 'fb_api_secret' ) );
	}

	/**
	 * get the user or page IDs saved in the database that the user wants to import
	 *
	 * @since 1.0
	 * @author jkudish
	 * @return array the user or page IDs
	 */
	public function get_user_or_page_ids() {
		$user_or_page_ids = Tribe__Settings_Manager::get_option( 'fb_user_or_page_ids', array() );
		if ( is_string( $user_or_page_ids ) ) {
			$user_or_page_ids = explode( "\n", $user_or_page_ids );
		}
		return apply_filters( 'tribe_fb_get_user_or_page_ids', $user_or_page_ids );
	}

	/**
	 * build the URL used to get the acccess token
	 *
	 * @since 1.0
	 * @author jkudish
	 * @return string the URL
	 */
	public function get_access_token_url() {
		$url = apply_filters( 'tribe_fb_get_access_token_url', add_query_arg( array(
				'grant_type' => 'client_credentials',
				'client_id' => $this->get_app_id(),
				'client_secret' => $this->get_app_secret(),
			), self::$fb_graph_url . 'oauth/access_token' ) );
		do_action( 'log', 'access token url', 'tribe-events-facebook', $url );
		return $url;
	}

	/**
	 * get the raw response access token
	 *
	 * @since 1.0
	 * @author jkudish
	 * @return string the access token
	 */
	public function get_raw_access_token() {
		$response = wp_remote_get( $this->get_access_token_url() );
		return wp_remote_retrieve_body( $response );
	}

	/**
	 * get the parsed access token
	 *
	 * @since 1.0
	 * @author jkudish
	 * @return string the access token
	 */
	public function get_access_token() {
		$data = json_decode( $this->get_raw_access_token() );
		$access_token = ! empty( $data->access_token ) ? $data->access_token : null;

		return apply_filters( 'tribe_fb_get_access_token', $access_token );
	}

	/**
	 * build out a graph url using query args and the FB access token
	 *
	 * @since 1.0
	 * @author jkudish
	 *
	 * @param string $path the path for the url
	 * @param array $query_args the arguments to use in building the query string
	 *
	 * @return string the full URL
	 */
	public function build_url_with_access_token( $path = '', $query_args = array() ) {
		$query_args = array_merge( $query_args, array( 'access_token' => $this->get_access_token() ) );
		$url = add_query_arg( $query_args, $this->get_graph_url() . $path );
		do_action( 'log', 'url with access token', 'tribe-events-facebook', $url );
		return $url;
	}

	/**
	 * retrieve a facebook object that is available at the root of the FB graph API
	 * example: https://graph.facebook.com/12345
	 *
	 * @param string $object_id the object to retrieve
	 *
	 * @return array the json data
	 */
	public function get_facebook_object( $object_id, $fields = array() ) {
		$url = $this->build_url_with_access_token( $object_id, $fields );

		$api = Tribe__Events__Facebook__API::instance();
		$this->fb_object = $api->json_retrieve( $url );

		return $this->fb_object;
	}

	/**
	 * retrieve a facebook event object that is available at the root of the FB graph API
	 * example: https://graph.facebook.com/12345
	 *
	 * @param string $object_id the object to retrieve
	 *
	 * @return array the json data
	 */
	public function get_facebook_event( $object_id ) {
		return $this->get_facebook_object(
			$object_id,
			array(
				'fields' => implode(
					',',
					array(
						'id',
						'cover',
						'description',
						'end_time',
						'name',
						'owner',
						'start_time',
						'ticket_uri',
						'timezone',
						'updated_time',
						'place',
					)
				),
			)
		);
	}

	/**
	 * Retrieve a facebook event photo url (after redirect) along with photo data.
	 *
	 * @param string $object_id
	 *
	 * @return mixed
	 */
	public function get_facebook_photo( $object_id ) {
		$photo_error = false;
		$api_url = $this->build_url_with_access_token( $object_id, array( 'fields' => 'cover' ) );

		do_action( 'log', 'api url', 'tribe-events-facebook', $api_url );

		$api = Tribe__Events__Facebook__API::instance();
		$response = $api->json_retrieve( $api_url );

		if ( isset( $response->cover ) && isset( $response->cover->source ) ) {
			$new_path = $response->cover->source;
			$get_photo = wp_remote_get( $response->cover->source );
		} else {
			$photo_error = true;
		}

		// Hat tip to @jessebrede (github) for suggesting a tweak to import old fb image formats if erroring
		if ( $photo_error || ! empty( $get_photo->errors ) ) {
			$api_url     = $this->build_url_with_access_token( $object_id . '/', array( 'fields' => 'picture', 'type' => 'large', 'return_ssl_resources' => 1 ) );

			$response    = $api->json_retrieve( $api_url );
			$new_path    = str_replace( '_q.', '_n.', $response->picture->data->url );
			$get_photo   = wp_remote_get( $new_path );
			$photo_error = false;
		}

		if ( ! is_wp_error( $get_photo ) && ! $photo_error ) {
			// setup return object
			$photo['url'] = $new_path;
			$photo['source'] = $get_photo['body'];
			return apply_filters( 'tribe_fb_get_facebook_photo', $photo, $api_url, $response );
		} else {
			if ( is_wp_error( $get_photo ) ) {
				$this->errors_images[] = $get_photo->get_error_message();
			} else {
				$this->errors_images[] = __( 'Could not successfully import the image for unknown reasons.', 'tribe-fb-import' );
			}
		}

		return false;
	}

	/**
	 * Requests for images from Facebook need an explicit width and height in order to get a reasonably sized
	 * non-square image back. This function tries to determine a size that can be scaled/cropped to fit all
	 * registered image sizes even where they each have a different ratio.
	 *
	 * @deprecated 3.4.1
	 * @return array [ width, height ]
	 */
	public function get_desired_img_dimensions() {
		// We need to access default and theme/plugin registered sizes
		$size_list = $GLOBALS['_wp_additional_image_sizes'];
		$size_labels = get_intermediate_image_sizes();

		// Reasonable defaults
		$width = 600;
		$height = 450;

		// We want to determine the biggest width and height we'll require to satisfy all sizes
		foreach ( $size_labels as $size ) {
			// Plugin/theme registered size? Check this first to avoid a db hit
			if ( isset( $size_list[ $size ]['width'] ) && isset( $size_list[ $size ]['width'] ) ) {
				$this_width = absint( $size_list[ $size ]['width'] );
				$this_height = absint( $size_list[ $size ]['height'] );
			}
			// Default size?
			else {
				$this_width = absint( get_option( $size . '_size_w' ) );
				$this_height = absint( get_option( $size . '_size_h' ) );
			}

			// Compare and keep the biggest
			$height = $this_height > $height ? $this_height : $height;
			$width = $this_width > $width ? $this_width : $width;
		}

		return array( $width, $height );
	}

	/**
	 * retrieve all events for a FB user or page
	 *
	 * @since 1.0
	 * @author jkudish
	 *
	 * @param string $user_or_page_id the ID to retrieve events for
	 * @param string $return what would you like to receive ( event IDs or objects )
	 *
	 * @return array the events
	 */
	public function get_events_for_object( $user_or_page_id, $return = 'id' ) {
		$valid_returns = array(
			'id',
			'object',
		);

		if ( ! in_array( $return, $valid_returns ) ) {
			_doing_it_wrong( 'get_events_for_object', esc_html__( 'The return argument should be object or id', 'tribe-fb-import' ), '1.0' );
			return false;
		}

		$args = array(
			'limit' => 9999,
			'since' => apply_filters( 'tribe_fb_import_since', date( 'Y-m-d' ), $user_or_page_id ),
		);

		$url = $this->build_url_with_access_token( $user_or_page_id . '/events', $args );

		$api = Tribe__Events__Facebook__API::instance();
		$data = (array) $api->retrieve_data_from( $url );

		if ( empty( $data ) || empty( $data[0] ) ) {
			return false;
		}

		$return_array = array();
		foreach ( $data as $event ) {
			if ( 'object' === $return ) {
				$return_array[] = $event;
			} else {
				$return_array[] = $event->id;
			}
		}
		return array_reverse( $return_array );
	}

	/**
	 * get all events for specified user or page ID
	 *
	 * @since 1.0
	 * @author jkudish
	 *
	 * @param array $user_or_page_ids the IDs to retrieve events for
	 * @param string $return what would you like to receive ( event IDs or objects )
	 *
	 * @return array the events
	 */
	public function get_events_for_specfic_user_or_page_ids( $user_or_page_ids = array(), $return = 'id' ) {
		$events = array();
		foreach ( $user_or_page_ids as $user_or_page_id ) {
			$events = array_merge( $events, $this->get_events_for_object( $user_or_page_id, $return ) );
		}
		return $events;
	}

	/**
	 * find a locally stored event/venue/organizer with the specified FB event ID
	 *
	 * @since 1.0
	 * @author jkudish
	 *
	 * @param string $facebook_id the Facebook ID of the event
	 * @param string $object_type the type of object we are looking for
	 * @param string $fallback_object_name the post title used as a fallback if $facebook_id is empty for some
	 *                                     reason
	 *
	 * @return int|null the event ID or null on failure
	 */
	public function find_local_object_with_fb_id( $facebook_id, $object_type = 'event', $fallback_object_name = null ) {
		switch ( $object_type ) {
			case 'event' :
				$meta_key = '_FacebookID';
				$post_type = Tribe__Events__Main::POSTTYPE;
				break;
			case 'organizer' :
				$meta_key = '_OrganizerFacebookID';
				$post_type = Tribe__Events__Main::ORGANIZER_POST_TYPE;
				break;
			case 'venue' :
				$meta_key = '_VenueFacebookID';
				$post_type = Tribe__Events__Main::VENUE_POST_TYPE;
				break;
			default :
				return new WP_Error( 'invalid_object_type', __( 'Object type provided is invalid', 'tribe-fb-import' ), $object_type );
		}

		$query = new WP_Query();
		$query_args = array(
			'post_type' => $post_type,
			'post_status' => 'any',
			'posts_per_page' => -1,
			'nopaging' => true,
			'update_post_term_cache' => false,
			'update_post_meta_cache' => false,
			'eventDisplay' => 'custom',
		);

		if ( false === $facebook_id ) {
			$query_args['name'] = sanitize_title( $fallback_object_name );
		} else {
			$query_args['meta_query'] = array(
				array(
					'key' => $meta_key,
					'value' => $facebook_id,
				),
			);
		}

		$query->query( $query_args );

		wp_reset_query();
		$post_id = ! empty( $query->posts[0] ) ? $query->posts[0]->ID : false;

		return apply_filters( 'tribe_fb_find_local_object_with_fb_id', $post_id );
	}

	/**
	 * parse the facebook venue given an object ID
	 * or use the venue property of the event itself
	 *
	 * @since 1.0
	 * @author jkudish
	 *
	 * @param object $facebook_event the Facebook event json object
	 * @see https://developers.facebook.com/docs/apps/upgrading#v22tov23
	 *
	 * @return object the venue object
	 */
	public function parse_facebook_venue( $facebook_event ) {
		$venue = (object) array(
			'facebook_id' => false,
			'name' => false,
			'description' => false,
			'address' => false,
			'city' => false,
			'country' => false,
			'zip' => false,
			'phone' => false,
		);

		$api = Tribe__Events__Facebook__API::instance();

		// On version 2.3 of Graph API Facebook doesn't have a Venue or Location, it's actually a "Page" object which has the location
		// We need to to a targeted API call to query that information, at least for now.
		if ( empty( $facebook_event->venue ) && empty( $facebook_event->location ) ){
			$url = $this->build_url_with_access_token( $facebook_event->id, array( 'fields' => 'place' ) );
			$event_place = $api->json_retrieve( $url );

			$venue->facebook_id = ( ! empty( $event_place->place->id ) ) ? trim( $event_place->place->id ) : false;
			$venue->name = ( ! empty( $event_place->place->name ) ) ? trim( $event_place->place->name ) : false;
			$venue->description = ( ! empty( $event_place->description ) ) ? trim( $event_place->description ) : false;
			$venue->address = ( ! empty( $event_place->place->location->street ) ) ? trim( $event_place->place->location->street ) : false;
			$venue->city = ( ! empty( $event_place->place->location->city ) ) ? trim( $event_place->place->location->city ) : false;
			$venue->state = ( ! empty( $event_place->place->location->state ) ) ? trim( $event_place->place->location->state ) : false;
			$venue->country = ( ! empty( $event_place->place->location->country ) ) ? trim( $event_place->place->location->country ) : false;
			$venue->zip = ( ! empty( $event_place->place->location->zip ) ) ? trim( $event_place->place->location->zip ) : false;
		} elseif ( ! empty( $facebook_event->venue->id ) ) {
			$raw_venue = $this->get_facebook_object( $facebook_event->venue->id );
			$venue->facebook_id = ( ! empty( $raw_venue->id ) ) ? trim( $raw_venue->id ) : $facebook_event->venue->id;
			$venue->name = ( ! empty( $raw_venue->name ) ) ? trim( $raw_venue->name ) : false;
			$venue->description = ( ! empty( $raw_venue->description ) ) ? trim( $raw_venue->description ) : false;
			$venue->address = ( ! empty( $raw_venue->location->street ) ) ? trim( $raw_venue->location->street ) : false;
			$venue->city = ( ! empty( $raw_venue->location->city ) ) ? trim( $raw_venue->location->city ) : false;
			$venue->state = ( ! empty( $raw_venue->location->state ) ) ? trim( $raw_venue->location->state ) : false;
			$venue->country = ( ! empty( $raw_venue->location->country ) ) ? trim( $raw_venue->location->country ) : false;
			$venue->zip = ( ! empty( $raw_venue->location->zip ) ) ? trim( $raw_venue->location->zip ) : false;
			$venue->phone = ( ! empty( $raw_venue->phone ) ) ? trim( $raw_venue->phone ) : false;
		} else {
			$venue->facebook_id = false;
			$venue->name = ( ! empty( $facebook_event->location ) ) ? trim( $facebook_event->location ) : false;
			$venue->description = false;
			$venue->address = ( ! empty( $facebook_event->venue->street ) ) ? trim( $facebook_event->venue->street ) : false;
			$venue->city = ( ! empty( $facebook_event->venue->city ) ) ? trim( $facebook_event->venue->city ) : false;
			$venue->state = ( ! empty( $facebook_event->venue->state ) ) ? trim( $raw_venue->venue->state ) : false;
			$venue->country = ( ! empty( $facebook_event->venue->country ) ) ? trim( $raw_venue->venue->country ) : false;
			$venue->zip = ( ! empty( $facebook_event->venue->zip ) ) ? trim( $facebook_event->venue->zip ) : false;
			$venue->phone = ( ! empty( $facebook_event->venue->phone ) ) ? trim( $facebook_event->venue->phone ) : false;
		}
		return $venue;
	}

	/**
	 * parse the facebook organizer given an object ID
	 * or use the owener property of the event itself
	 *
	 * @since 1.0
	 * @author jkudish
	 *
	 * @param object $facebook_event the Facebook event json object
	 *
	 * @return object the organizer object
	 */
	public function parse_facebook_organizer( $facebook_event ) {
		$organizer = new stdClass;
		if ( ! empty( $facebook_event->owner->id ) ) {
			$raw_organizer = $this->get_facebook_object( $facebook_event->owner->id );
			$organizer->facebook_id = ! empty( $raw_organizer->id ) ? trim( $raw_organizer->id ) : trim( $facebook_event->owner->id );
			$organizer->name = ! empty( $raw_organizer->name ) ? trim( $raw_organizer->name ) : trim( $facebook_event->owner->name );
			$organizer->phone = ! empty( $raw_organizer->phone ) ? trim( $raw_organizer->phone ) : false;
			$organizer->website = ! empty( $raw_organizer->link ) ? trim( $raw_organizer->link ) : false;
			$organizer->email = ! empty( $raw_organizer->email ) ? trim( $raw_organizer->email ) : false;
		} else {
			$organizer->facebook_id = false;
			$organizer->name = ! empty( $facebook_event->owner->name ) ? trim( $facebook_event->owner->name ) : false;
			$organizer->phone = ! empty( $facebook_event->owner->phone ) ? trim( $facebook_event->owner->phone ) : false;
			$organizer->website = ! empty( $facebook_event->owner->website ) ? trim( $facebook_event->owner->website ) : false;
			$organizer->email = ! empty( $facebook_event->owner->email ) ? trim( $facebook_event->owner->email ) : false;
		}
		return $organizer;
	}

	/**
	 * parse a facebook event to get all the necessary
	 * params to create the local event
	 *
	 * @since 1.0
	 * @author jkudish
	 *
	 * @param object $facebook_event the Facebook event json object
	 *
	 * @return array the event paramaters
	 */
	public function parse_facebook_event( $facebook_event ) {
		// Obtain the organizer and venue objects (if set)
		$organizer = $this->parse_facebook_organizer( $facebook_event );
		$venue = $this->parse_facebook_venue( $facebook_event );

		// We want to test and see if the organizer/venue is already stored in the posts table
		$local_organizer_id = null;
		$local_venue_id = null;

		if ( false !== $organizer->facebook_id ) {
			$local_organizer_id = $this->find_local_object_with_fb_id( $organizer->facebook_id, 'organizer', $organizer->name );
		}

		if ( false !== $venue->facebook_id ) {
			$local_venue_id = $this->find_local_object_with_fb_id( $venue->facebook_id, 'venue', $venue->name );
		}

		// Setup the base array
		$event_params = array(
			'FacebookID' => $facebook_event->id,
			'post_title' => ( ! empty( $facebook_event->name ) ) ? $facebook_event->name : '',
			'post_status' => Tribe__Events__Importer__Options::get_default_post_status( 'facebook' ),
			'post_content' => ( ! empty( $facebook_event->description ) ) ? make_clickable( $facebook_event->description ) : '',
		);

		// Set organizer only if no local organizer exists
		if ( empty( $local_organizer_id ) && ! empty( $organizer->name ) ) {
			$event_params['Organizer'] = array(
				'FacebookID' => $organizer->facebook_id,
				'Organizer' => $organizer->name,
				'Phone' => $organizer->phone,
				'Website' => $organizer->website,
				'Email' => $organizer->email,
			);
		}

		// Set organizer ID
		if ( ! empty( $local_organizer_id ) ) {
			$event_params['EventOrganizerID'] = $local_organizer_id;
		}

		// Set venue only if local venue is empty
		if ( empty( $local_venue_id ) && ! empty( $venue->name ) ) {
			$event_params['Venue'] = array(
				'FacebookID' => $venue->facebook_id,
				'Venue' => $venue->name,
				'Address' => $venue->address,
				'City' => $venue->city,
				'StateProvince' => $venue->state,
				'Country' => $venue->country,
				'Zip' => $venue->zip,
				'Phone' => $venue->phone,
			);
		}

		// Set venue ID
		if ( ! empty( $local_venue_id ) ) {
			$event_params['EventVenueID'] = $local_venue_id;
		}

		// The end time may not explicitly be set (it may be undefined)
		if ( empty( $facebook_event->end_time ) ) {
			$facebook_event->end_time = $facebook_event->start_time;
		}

		// Normalize the date formats
		$start_time = $this->extract_db_datetime( $facebook_event->start_time );
		$end_time   = $this->extract_db_datetime( $facebook_event->end_time );

		// Set the dates
		if ( isset( $facebook_event->all_day ) && $facebook_event->all_day ) {
			$event_params['EventStartDate'] = tribe_beginning_of_day( $start_time );
			$event_params['EventEndDate']   = tribe_end_of_day( $start_time );
			$event_params['EventAllDay'] = 'yes';
		} else {
			$event_params['EventStartDate'] = Tribe__Date_Utils::date_only( $start_time );
			$event_params['EventEndDate']   = Tribe__Date_Utils::date_only( $end_time );
			$event_params['EventStartHour'] = Tribe__Date_Utils::hour_only( $start_time );
			$event_params['EventStartMinute'] = Tribe__Date_Utils::minutes_only( $start_time );
			$event_params['EventStartMeridian'] = Tribe__Date_Utils::meridian_only( $start_time );
			$event_params['EventEndHour'] = Tribe__Date_Utils::hour_only( $end_time );
			$event_params['EventEndMinute'] = Tribe__Date_Utils::minutes_only( $end_time );
			$event_params['EventEndMeridian'] = Tribe__Date_Utils::meridian_only( $end_time );
		}

		$event_params['EventTimezone'] = $this->extract_utc_offset( $facebook_event->start_time );

		return apply_filters( 'tribe_fb_parse_facebook_event', $event_params );
	}

	/**
	 * Passing an ISO 8601 style datetime complete with offset can result in
	 * "unexpected" results when retrieving the hour, etc, via Tribe__Date_Utils.
	 * This utility function essentially strips that.
	 *
	 * @param string $iso8601_datetime
	 *
	 * @return string
	 */
	protected function extract_db_datetime( $iso8601_datetime ) {
		try {
			$datetime = new DateTime( $iso8601_datetime );
			return $datetime->format( Tribe__Date_Utils::DBDATETIMEFORMAT );
		}
		catch ( Exception $e ) {
			return $datetime;
		}
	}

	/**
	 * Given an ISO 8601 format datetime string extracts and returns the timezone element,
	 * converting it to "UTC-x" style.
	 *
	 * @param string $iso8601_datetime
	 *
	 * @return string
	 */
	public function extract_utc_offset( $iso8601_datetime ) {
		try {
			$datetime = new DateTime( $iso8601_datetime );
		} catch ( Exception $e ) {
			return '';
		}

		$timezone = $datetime->getTimezone();
		$offset   = $timezone->getOffset( $datetime ) / 60 / 60;

		if ( $offset >= 0 ) {
			$offset = '+' . $offset;
		}

		return 'UTC' . $offset;
	}

	/**
	 * Create or update an event given a Facebook ID
	 *
	 * @param int  $facebook_event_id the Facebook ID of the event
	 * @param bool $update_existing   whether to update the event if it was previously imported
	 *
	 * @return array|WP_Error
	 * @author jkudish
	 * @since 1.0
	 */
	public function create_local_event( $facebook_event_id, $update_existing = false ) {
		$existing_id = $this->find_local_object_with_fb_id( $facebook_event_id, 'event' );

		// If the event has already been imported then report an error and skip (unless $update_existing is true)
		if ( $existing_id && ! $update_existing ) {
			$message = sprintf( __( 'Event ID "%s" was already imported from Facebook.', 'tribe-fb-import' ), $facebook_event_id );
			return new WP_Error( 'event_already_exists', $message );
		}

		// Try to load the Facebook event
		$facebook_event = $this->get_facebook_event( $facebook_event_id );

		// If unsuccessful, return an appropriate error
		if ( ! isset( $facebook_event->id ) ) {
			do_action( 'log', 'Facebook event', 'tribe-events-facebook', $facebook_event );
			$message = sprintf( __( "Either the event with ID %s does not exist, is marked as private on Facebook or we couldn't reach the Facebook API", 'tribe-fb-import' ), $facebook_event_id );
			return new WP_Error( 'invalid_event', $message );
		}

		// Is this an all day event?
		$this->is_all_day( $facebook_event );

		// Parse the event
		$args = $this->parse_facebook_event( $facebook_event );

		// Filter the origin trail
		add_filter( 'tribe-post-origin', array( $this, 'origin_filter' ) );

		// Create the event or update the existing one
		if ( $existing_id ) {
			unset( $args[ 'post_status' ] ); // No need to reset the post status when updating existing events
			$event_id = tribe_update_event( $existing_id, $args );
		} else {
			$event_id = tribe_create_event( $args );
		}

		// Count this as a successful import
		$this->imported_total++;

		// Import the event image (if set)
		$this->import_event_image( $event_id, $facebook_event );

		// Set the event's Facebook ID meta
		update_post_meta( $event_id, '_FacebookID', $args['FacebookID'] );

		// Set the event's map status if global setting is enabled
		if ( tribe_get_option( 'fb_enable_GoogleMaps' ) ) {
			update_post_meta( $event_id, '_EventShowMap', true );
		}

		// Get the created organizer/venue IDs
		$organizer_id = tribe_get_organizer_id( $event_id );
		$venue_id = tribe_get_venue_id( $event_id );

		// Set the post status to publish for the organizer and venue.
		if ( ! empty( $organizer_id ) && 'publish' !== get_post_status( $organizer_id ) ) {
			$this->publish_post( $organizer_id );
		}

		if ( ! empty( $venue_id ) && 'publish' !== get_post_status( $venue_id ) ) {
			$this->publish_post( $venue_id );
		}

		// Set organizer Facebook ID
		if ( isset( $args['Organizer']['FacebookID'] ) ) {
			update_post_meta( $organizer_id, '_OrganizerFacebookID', $args['Organizer']['FacebookID'] );
		}

		// Set venue Facebook ID
		if ( isset( $args['Venue']['FacebookID'] ) ) {
			update_post_meta( $venue_id, '_VenueFacebookID', $args['Venue']['FacebookID'] );
		}

		// Remove filter for the origin trail
		remove_filter( 'tribe-post-origin', array( $this, 'origin_filter' ) );

		/**
		 * Fire a create event action when we create a local event
		 *
		 * @param int $event_id
		 * @param int $organizer_id
		 * @param int $venue_id
		 */
		do_action( 'tribe_events_facebook_event_created', $event_id, $organizer_id, $venue_id );

		return array( 'event' => $event_id, 'organizer' => $organizer_id, 'venue' => $venue_id );
	}

	/**
	 * Attempts to pull in the event image, if there is one, and attach it to the
	 * specified event post.
	 *
	 * @param $event_id
	 * @param $facebook_event
	 */
	protected function import_event_image( $event_id, $facebook_event ) {
		// Attempt to grab the event image
		$event_picture = apply_filters( 'tribe_fb_event_img', $this->get_facebook_photo( $facebook_event->id ), $facebook_event->id );

		// If there was a problem bail out: any error messages will have been added by get_facebook_photo()
		if ( false === $event_picture ) {
			return;
		}

		// Prep to import
		$url         = $this->clean_img_url( $event_picture['url'] );
		$uploads     = wp_upload_dir();
		$wp_filetype = wp_check_filetype( $url, null );
		$filename    = wp_unique_filename( $uploads['path'], basename( 'facebook_event_' . $facebook_event->id ), $unique_filename_callback = null ) . '.' . $wp_filetype['ext'];
		$full_path   = $uploads['path'] . '/' . $filename;

		// Do we have an acceptable filetype?
		if ( ! substr_count( $wp_filetype['type'], 'image' ) ) {
			$this->errors_images[] = sprintf( __( '%1$s. Event Image Error: "%2$s" is not a valid image. %3$s', 'tribe-fb-import' ), $this->fb_object->name, basename( $url ), $wp_filetype['type'] );
			return;
		}

		// Try to save the image to the uploads dir
		$file_saved = file_put_contents( $uploads['path'] . '/' . $filename, $event_picture['source'] );

		if ( ! $file_saved ) {
			$this->errors_images[] = sprintf( __( '%s. Event Image Error: The file cannot be saved.', 'tribe-fb-import' ), $this->fb_object->name );
			return;
		}

		// Attach to the event
		$attachment = array(
			'post_mime_type' => $wp_filetype['type'],
			'post_title' => preg_replace( '/\.[^.]+$/', '', $filename ),
			'post_content' => '',
			'post_status' => 'inherit',
			'guid' => $uploads['url'] . '/' . $filename,
		);

		$attach_id = wp_insert_attachment( $attachment, $full_path, $event_id );

		if ( ! $attach_id ) {
			$this->errors_images[] = sprintf( __( '%s. Event Image Error: Failed to save record into database.', 'tribe-fb-import' ), $this->fb_object->name );
			return;
		}

		// Set as featured image
		set_post_thumbnail( $event_id, $attach_id );

		// Attach attachment metadata
		require_once ABSPATH . 'wp-admin/includes/image.php';
		$attach_data = wp_generate_attachment_metadata( $attach_id, $full_path );
		wp_update_attachment_metadata( $attach_id, $attach_data );
	}

	/**
	 * Removes the query segment of the provided URL, if there is one.
	 *
	 * Facebook image URLs commonly (though not always) contain a query, for example:
	 *
	 *     http://cdn.fb.com/event/image-name.jpg?abc=123&def=456
	 *
	 * We use the URL as the basis of the image filename when importing, however the
	 * query segment can cause issues when it is passed through wp_check_filetype(),
	 * which is then unable to determine the file extension. For that reason, this method
	 * primarily exists to strip out the URL query.
	 *
	 * @param  string $url
	 *
	 * @return string
	 */
	protected function clean_img_url( $url ) {
		$query = parse_url( $url, PHP_URL_QUERY );

		// On occasion there may be no query to strip
		if ( is_string( $query ) ) {
			$url = str_replace( "?$query", '', $url );
		}

		return stripslashes( $url );
	}

	/**
	 * Assesses if the $facebook_event object is representing an all day event
	 * and adds an all_day property to reflect the outcome.
	 *
	 * The Facebook API can be something of a moving target and so the logic
	 * used to detect if an event is an "all day" one or not can be overridden
	 * using the following filter:
	 *
	 *     tribe_fb_determine_if_is_all_day
	 *
	 * @param &$facebook_event
	 */
	protected function is_all_day( &$facebook_event ) {
		$end_time = isset( $facebook_event->end_time ) ? $facebook_event->end_time : null;

		// At this time, an all day event is characterized by starting at 12.00am but having no end time
		if ( isset( $facebook_event->start_time ) && ! isset( $facebook_event->end_time ) && $this->is_midnight( $facebook_event->start_time ) ) {
			$facebook_event->all_day = true;
			$facebook_event->end_time = $facebook_event->start_time;
		}
		else {
			$facebook_event->all_day = false;
		}

		$facebook_event->all_day = apply_filters( 'tribe_fb_determine_if_is_all_day',
			$facebook_event->all_day, $facebook_event->start_time, $end_time, $facebook_event->id );
	}

	/**
	 * Tests to determine if the date/time string represents midnight.
	 *
	 * @param $time
	 *
	 * @return bool
	 */
	protected function is_midnight( $time ) {
		try {
			$time = new DateTime( $time );
			$time = $time->format( Tribe__Date_Utils::TIMEFORMAT );

			return ( $time === '12:00 AM' );
		}
		catch ( Exception $e ) {
			return false;
		}
	}

	/**
	 * Used to publish a post type and ensure it has a post name (slug).
	 *
	 * When the importer is set to save imported events as drafts, new venues and organizers will initially be
	 * created as drafts, too - but will not have a post_name. Simply calling wp_publish_post() doesn't cause
	 * this to happen automatically and so this method takes care of that additional step.
	 *
	 * @param int $id post ID (for the venue/organizer, typically)
	 */
	protected function publish_post( $id ) {
		$post = get_post( $id );

		// Ensure the post_name is populated
		if ( empty( $post->post_name ) ) {
			$post_name = sanitize_title( $post->post_title );
			wp_update_post( array( 'ID' => $id, 'post_name' => $post_name ) );
		}

		wp_publish_post( $id );
	}


	/**
	 * origin/trail filter
	 *
	 * @since 1.0
	 * @author jkudish
	 * @return string facebook importer identifier
	 */
	public function origin_filter() {
		return self::$origin;
	}

	/**
	 * returns an array of fb_uids given a text blob of them
	 *
	 * @since 1.0
	 * @author jkudish
	 *
	 * @param string $fb_uids the raw Facebook identifiers
	 *
	 * @return array the Facebook identifiers
	 */
	public function parse_fb_uids( $fb_uids ) {
		return array_map( 'trim', (array) explode( "\n", tribe_multi_line_remove_empty_lines( $fb_uids ) ) );
	}

	/**
	 * returns an array of FB event IDs given a text blob of them
	 *
	 * @since 1.0
	 * @author jkudish
	 *
	 * @param string $raw_event_ids the raw Facebook event IDs
	 *
	 * @return array the parsed Facebook event IDs
	 */
	public function parse_events_from_textarea( $raw_event_ids ) {
		if ( ! preg_match( '/^[0-9\s]+$/', $raw_event_ids ) ) {
			$this->errors[] = __( 'The Facebook event IDs provided must be numeric and one per line.', 'tribe-fb-import' );
			return array();
		}

		return array_map( 'trim', (array) explode( "\n", tribe_multi_line_remove_empty_lines( $raw_event_ids ) ) );
	}

	/**
	 * offset_date_to_timezone useful for adjusting an imported DateTime to your site GMT/UTC offset
	 *
	 * @link https://gist.github.com/4704496
	 *
	 * @param string $datetime
	 * @param string $timezone default GMT
	 * @param string $return_format default ISO 8601
	 *
	 * @return string $datetime as $return_format
	 */
	public function offset_date_to_timezone( &$datetime, $timezone = 'GMT', $return_format = 'c' ) {
		_deprecated_function( 'Tribe__Events__Facebook__Importer::offset_date_to_timezone', '3.12' );

		// get site timezone offset
		$gmt_offset = get_option( 'gmt_offset' );
		$gmt_offset = str_replace( array( '.25', '.5', '.75' ), array( ':15', ':30', ':45' ), $gmt_offset );

		// set DateTime obj
		$datetime_obj = new DateTime( $datetime, new DateTimeZone( $timezone ) );

		// reset date timezone to be neutral for offset
		$datetime_obj->setTimezone( new DateTimeZone( 'GMT' ) );

		// modify the timezone with offset
		$datetime_obj->modify( $gmt_offset . ' hours' );

		// return start_time & end_time as ISO 8601 date per https://developers.facebook.com/docs/reference/api/event/
		$datetime = $datetime_obj->format( $return_format );
	}

	/**
	 * generate the import page
	 *
	 * @since 1.0
	 * @author jkudish
	 * @return void
	 */
	public function do_import_page() {
		$this->process_import_page();
		include( self::$plugin_root . 'src/admin-views/import-page.php' );
	}

	/**
	 * process import when submitted
	 *
	 * @since 1.0
	 * @author jkudish
	 * @return void
	 */
	public function process_import_page() {
		if ( ! empty( $_POST['tribe-confirm-import'] ) ) {
			// check nonce
			check_admin_referer( 'tribe-fb-import', 'tribe-confirm-import' );

			$events_to_import = array();
			$this->no_events_imported = true;

			// checked events from list
			if ( ! empty( $_POST['tribe-fb-import-events'] ) ) {
				$events_to_import = array_merge( $events_to_import, $_POST['tribe-fb-import-events'] );
			}

			// individual events from textarea
			if ( ! empty( $_POST['tribe-fb-import-events-by-id'] ) ) {
				$events_to_import = array_merge( $events_to_import, $this->parse_events_from_textarea( $_POST['tribe-fb-import-events-by-id'] ) );
			}
			// loop through events and import them
			if ( ! empty( $events_to_import ) && empty( $this->errors ) ) {
				foreach ( $events_to_import as $facebook_event_id ) {
					$local_event = $this->create_local_event( $facebook_event_id );
					do_action( 'log', 'local event', 'tribe-events-facebook', $local_event );
					if ( is_wp_error( $local_event ) ) {
						$this->errors[] = $local_event->get_error_message();
					} else {
						$this->no_events_imported = false;
					}
				}
			} else {
				$this->errors[] = __( 'No valid events were provided for import. The import failed as a result.', 'tribe-fb-import' );
			}

			// mark it as successful
			if ( empty( $this->errors ) ) {
				$this->success = true;
			}
		}
	}

	/**
	 * generate/display the fields used in the import page
	 *
	 * @since 1.0
	 * @author jkudish
	 *
	 * @param string $fb_uids the Facebook identifiers (parsed into an array) to generate the import fields with
	 *
	 * @return void
	 */
	public function build_import_fields( $fb_uids ) {
		foreach ( $this->parse_fb_uids( $fb_uids ) as $fb_uid ) {
			// Skip if we can't load a valid FB object
			$this->get_facebook_object( $fb_uid );
			if ( ! $this->valid_fb_response( $fb_uid ) ) {
				continue;
			}

			// If a name is associated with it, then use it as the title
			if ( ! empty( $this->fb_object->name ) ) {
				echo '<h4>' . sprintf( esc_html_x( 'Events from %s:', '%s is the name of the Facebook user or page', 'tribe-fb-import' ), esc_html( $this->fb_object->name ) ) . '</h4>';
			}

			// Is this a person/individual? Explain that isn't allowed then skip to the next item
			if ( $this->is_person() ) {
				$name = ! empty( $this->fb_object->name ) ? $this->fb_object->name : $fb_uid;
				echo '<p>' . sprintf( esc_html__( '%s appears to be an individual. At this time events can only be fetched from organizations and pages, not individuals. If you would like to import an event from an individual, you can do so by entering the Facebook event ID in the box below.', 'tribe-fb-import' ), esc_html( $name ) );
				continue;
			}

			// Print the list of events ready for import
			$this->list_events();
		}
	}

	/**
	 * Did Facebook respond with a valid object?
	 *
	 * If a specific error was returned by Facebook then this will be printed directly.
	 *
	 * @return bool
	 */
	protected function valid_fb_response( $fb_uid ) {
		$valid = ! empty( $this->fb_object ) && ! is_wp_error( $this->fb_object );

		if ( ! empty( $this->fb_object->error ) ) {
			$valid = false;

			if ( strpos( $this->fb_object->error->message, 'Unsupported get request' ) !== false ) {

				echo sprintf( __( 'Events from <b>%s</b> are not importing because Facebook has returned an error, "Unsupported get request." That error is usually due to an age or country restriction in that Facebook Page\'s settings, please see our %sFacebook API Errors%s article for options to resolve this.', 'tribe-fb-import' ), esc_html( $fb_uid ), '<a href="http://m.tri.be/s1" target="blank">', '</a>' );
			} else {

				echo sprintf( __( 'Facebook API Error returned for <b>%s</b>: %s', 'tribe-fb-import' ), esc_html( $fb_uid ), $this->fb_object->error->message );
			}
		}

		return $valid;
	}

	/**
	 * Tries to determine if the provided Facebook object relates to an individual, rather than an
	 * organization or business, etc.
	 *
	 * @return bool
	 */
	protected function is_person() {
		return isset( $this->fb_object->gender );
	}

	/**
	 * Print a list of all events relating to the current Facebook entity.
	 */
	protected function list_events() {
		$fb_events = $this->get_events_for_object( $this->fb_object->id, 'object' );

		if ( ! empty( $fb_events ) ) {
			echo '<ul>';

			foreach ( $fb_events as $fb_event ) {
				$html_id = esc_attr( 'tribe-fb-import-event-' . $fb_event->id );
				$event_exists_locally = $this->find_local_object_with_fb_id( $fb_event->id );
				$imported = ( $event_exists_locally ) ? ' ' . __( '(previously imported)', 'tribe-fb-import' ) : '';
				$start_date = date_i18n( $this->date_format, strtotime( $fb_event->start_time ) );
				$title = $start_date . ' &mdash; ' . $fb_event->name . $imported;
				$title = apply_filters( 'tribe_fb_event_checkbox_label', $title, $start_date, $fb_event, $imported );
				echo '<li><label for="' . esc_attr( $html_id ) . '"><input class="checkbox" name="tribe-fb-import-events[]" type="checkbox" id="' . esc_attr( $html_id ) . '" type="tribe-fb-import-event" value="' . esc_attr( $fb_event->id ) . '" ';
				checked( (bool) $event_exists_locally );
				disabled( (bool) $event_exists_locally );
				echo '> ' . $title . '</label> (<a href="https://www.facebook.com/events/' . esc_attr( $fb_event->id ) . '/" target="_blank">link</a>)</li>';
			}

			echo '</ul>';
		}
		elseif ( ! empty( $this->fb_object->name ) ) {
			echo '<p>' . sprintf( esc_html_x( '%s does not have any Facebook events', '%s is the name of the Facebook user or page', 'tribe-fb-import' ), esc_html( $this->fb_object->name ) ) . '</p>';
		}
	}

	/**
	 * Handles the automatic import process.
	 */
	public function do_auto_import() {
		// Grab a list of FB UIDs we want to import from and a list of previously deleted FB events
		$fb_uids = $this->parse_fb_uids( Tribe__Settings_Manager::get_option( 'fb_uids' ) );
		$deleted = get_option( 'tribe_facebook_deleted_ids', array() );

		// Mark the Last Import Timing
		set_transient( 'tribe_facebook_last_auto_import', time(), 0 );

		// Loop through each valid FB UID
		foreach ( $fb_uids as $id ) {

			// Try to load the corresponding FB object: skip if we hit problems
			$facebook_object = $this->get_facebook_object( $id );
			if ( empty( $facebook_object ) || is_wp_error( $facebook_object ) ) {
				continue;
			}

			// Load the events (skip to the next FB entity if there are none)
			$fb_events = $this->get_events_for_object( $facebook_object->id, 'object' );
			if ( empty( $fb_events ) ) {
				continue;
			}

			// Attempt to import each event not included in the list of deleted FB IDs
			foreach ( $fb_events as $fb_event ) {
				if ( in_array( $fb_event->id, $deleted ) ) {
					continue;
				}
				$this->create_local_event( $fb_event->id );
			}
		}
	}

	/**
	 * create a scheduled event when the facebook settings tab is saved
	 *
	 * @since 1.0
	 * @author jkudish
	 * @return void
	 */
	public function manage_import_schedule() {
		wp_clear_scheduled_hook( 'tribe_fb_auto_import' );
		if ( Tribe__Settings_Manager::get_option( 'fb_auto_import' ) ) {
			$recurrence = Tribe__Settings_Manager::get_option( 'fb_auto_frequency' );
			if ( array_key_exists( $recurrence, self::$valid_reccurence_patterns ) ) {
				wp_schedule_event( $this->get_first_occurence_timestamp( $recurrence ), $recurrence, 'tribe_fb_auto_import' );
			}
		}
	}

	/**
	 * given a recurrence pattern, returns the timestamp of
	 * the first occurrence
	 *
	 * @since 1.0
	 * @author jkudish
	 *
	 * @param string $recurrence the recurrence pattern
	 *
	 * @return int timestamp
	 */
	public function get_first_occurence_timestamp( $recurrence ) {
		if ( array_key_exists( $recurrence, self::$valid_reccurence_patterns ) ) {
			return intval( time() + self::$valid_reccurence_patterns[ $recurrence ] );
		}
	}

	/**
	 * add a weekly 'schedule' to the WP Cron API
	 *
	 * @since 1.0
	 * @author jkudish
	 *
	 * @param array $schedules the existing schedules in the WP Cron API
	 *
	 * @return array the schedules in the WP Cron API
	 */
	public function cron_add_weekly( $schedules ) {
		if ( ! isset( $schedules['weekly'] ) ) {
			$schedules['weekly'] = array(
				'interval' => 604800,
				'display' => __( 'Once Weekly', 'tribe-fb-import' ),
			);
		}
		return $schedules;
	}

	/**
	 * Add the facebook importer toolbar item.
	 *
	 * @since 1.0
	 * @author PaulHughes01
	 * @return void
	 */
	public function addFacebookToolbarItems() {
		global $wp_admin_bar;

		if ( current_user_can( 'publish_tribe_events' ) ) {
			$import_node = $wp_admin_bar->get_node( 'tribe-events-import' );
			if ( ! is_object( $import_node ) ) {
				$wp_admin_bar->add_menu( array(
					'id' => 'tribe-events-import',
					'title' => __( 'Import', 'tribe-fb-import' ),
					'parent' => 'tribe-events-import-group',
				) );
			}
		}

		if ( current_user_can( 'publish_tribe_events' ) ) {
			$wp_admin_bar->add_menu( array(
				'id' => 'tribe-facebook-import',
				'title' => __( 'Facebook', 'tribe-fb-import' ),
				'href' => Tribe__Events__Importer__Admin_Page::instance()->get_url( array( 'tab' => 'facebook' ) ),
				'parent' => 'tribe-events-import',
			) );
		}

	}

	/**
	 * Return additional action for the plugin on the plugins page.
	 *
	 * @param array $actions
	 *
	 * @since 2.0.8
	 * @return array
	 */
	public function addLinksToPluginActions( $actions ) {
		if ( class_exists( 'Tribe__Events__Main' ) ) {
			$actions['settings'] = '<a href="' . Tribe__Events__Importer__Admin_Page::instance()->get_url( array( 'tab' => 'facebook' ) ) .'">' . __( 'Settings', 'tribe-fb-import' ) . '</a>';
		}
		return $actions;
	}

	/**
	 * Sets deleted events that were imported from Facebook so that it does not re-import them.
	 *
	 * @param int $post_id
	 *
	 * @since 1.0.2
	 * @author PaulHughes01
	 * @return void
	 */
	public function setDeletedEventArrayOption( $post_id ) {
		if ( get_post_type( $post_id ) == Tribe__Events__Main::POSTTYPE
			&& get_post_meta( $post_id, '_EventOrigin', true ) == self::$origin
			&& get_post_meta( $post_id, '_FacebookID', true ) != '' ) {

			$facebook_event_id = get_post_meta( $post_id, '_FacebookID', true );
			$deleted_ids = get_option( 'tribe_facebook_deleted_ids', array() );
			if ( ! in_array( $facebook_event_id, $deleted_ids ) ) {
				$deleted_ids[] = $facebook_event_id;
				update_option( 'tribe_facebook_deleted_ids', $deleted_ids );
			}
		}
	}

	/**
	 * Return the forums link as it should appear in the help tab.
	 *
	 * @param string $unused_content
	 *
	 * @return string
	 * @since 1.0.2
	 */
	public function helpTabForumsLink( $unused_content ) {
		$promo_suffix = '?utm_source=helptab&utm_medium=plugin-facebook&utm_campaign=in-app';

		return ( isset( Tribe__Events__Main::$tecUrl ) ? Tribe__Events__Main::$tecUrl : Tribe__Events__Main::$tribeUrl ) . 'support/forums/' . $promo_suffix;
	}

	/**
	 * display a failure message when TEC is not installed
	 *
	 * @since 1.0
	 * @author jkudish
	 * @return void
	 */
	public static function fail_message() {
		if ( current_user_can( 'activate_plugins' ) ) {
			$url = add_query_arg( array( 'tab' => 'plugin-information', 'plugin' => 'the-events-calendar', 'TB_iframe' => 'true' ), admin_url( 'plugin-install.php' ) );
			$title = __( 'The Events Calendar', 'tribe-fb-import' );
			echo '<div class="error"><p>' . sprintf( esc_html__( 'To begin using The Events Calendar: Facebook Events, please install the latest version of %s.', 'tribe-fb-import' ), '<a href="' . esc_url( $url ) . '" class="thickbox" title="' . esc_attr( $title ) . '">' . $title . '</a>', $title ) . '</p></div>';
		}
	}

	/**
	 * Add FB Importer to the list of add-ons to check required version.
	 *
	 * @param array $plugins the existing plugins
	 *
	 * @return mixed
	 * @author jkudish
	 * @since 1.0
	 */
	public static function init_addon( $plugins ) {
		$plugins['TribeFBImporter'] = array(
			'plugin_name' => 'The Events Calendar: Facebook Events',
			'required_version' => self::REQUIRED_TEC_VERSION,
			'current_version' => self::VERSION,
			'plugin_dir_file' => basename( dirname( __FILE__ ) ) . '/the-events-calendar-facebook-importer.php',
		);
		return $plugins;
	}

	/**
	 * static singleton method
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) ) {
			$className = __CLASS__;
			self::$instance = new $className;
		}
		return self::$instance;
	}
}
