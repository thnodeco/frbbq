<?php
/**
 * Facilitates syncronizing a previously imported local copy of an event with the
 * possibly updated source event on facebook.com.
 *
 * For example, if the even description or time is changed on Facebook then the
 * resync tool will provide a straightforward means of letting the user update their
 * local copy.
 */
class Tribe__Events__Facebook__Resync {
	protected $facebook_id;


	public function __construct() {
		add_action( 'add_meta_boxes_' . Tribe__Events__Main::POSTTYPE, array( $this, 'add_meta_box' ) );
		$this->listen();
	}

	/**
	 * Setup a new meta box for our re-sync control that should display in the event editor.
	 */
	public function add_meta_box() {
		global $post_id;

		// If this event is not linked to a facebook.com event then we do probably do not need the meta box
		$show_metabox = (bool) $this->fb_id( $post_id );

		/**
		 * Control whether the resync meta box should display or not.
		 *
		 * @var bool $show_metabox
		 * @var int  $post_id
		 */
		$show_metabox = apply_filters( 'tribe_facebook_show_resync_meta_box', $show_metabox, $post_id );

		if ( ! $show_metabox ) {
			return;
		}

		add_meta_box(
			'facebook_resync',
			_x( 'Facebook', 'resync meta box title', 'tribe-fb-import' ),
			array( $this, 'do_meta_box' ),
			Tribe__Events__Main::POSTTYPE,
			'side'
		);
	}

	/**
	 * Render the re-sync meta box.
	 */
	public function do_meta_box() {
		$check = esc_attr( wp_create_nonce( 'fb_resync_event' ) );
		$button_text = esc_html__( 'Re-synchronize', 'tribe-fb-import' );
		$helper_text = esc_html__( 'If you wish to update this event to match any changes that may have been made over on facebook.com, please use the provided button.', 'tribe-fb-import' );

		echo "
			<p>
				<button name='facebook_resync' class='button-primary' value='$check'> $button_text </button>
			</p> <p>
				$helper_text
			</p>
		";
	}

	/**
	 * If the user has requested we resyncronize then validate their request and make it so.
	 */
	protected function listen() {
		// Has the user requested we resync the event?
		if ( empty( $_POST[ 'post_ID' ] ) || empty( $_POST[ 'facebook_resync' ] ) ) {
			return;
		}

		// Security - does the nonce check out?
		if ( ! wp_verify_nonce( $_POST[ 'facebook_resync' ], 'fb_resync_event' ) ) {
			return;
		}

		// Sanity - is this an event post and is it linked to a Facebook event?
		$this->facebook_id = $this->fb_id( $_POST[ 'post_ID' ] );

		if ( ! tribe_is_event( $_POST[ 'post_ID' ] ) || ! $this->facebook_id ) {
			return;
		}

		// Wait until after WP's editpost action has completed (to avoid the resync being wiped)
		add_action( 'save_post_'. Tribe__Events__Main::POSTTYPE, array( $this, 'resync' ), 100 );
	}

	/**
	 * Expects to run late on save_post_tribe_events.
	 *
	 * After WP has updated the post based on the contents of the editor fields (many of which do
	 * not map to corresponding fields on Facebook), perform the resync.
	 */
	public function resync() {
		remove_action( 'save_post_'. Tribe__Events__Main::POSTTYPE, array( $this, 'resync' ), 100 );
		Tribe__Events__Facebook__Importer::instance()->create_local_event( $this->facebook_id, true );
	}

	/**
	 * Returns the Facebook ID associated with the specified event (if any).
	 *
	 * If the Facebook ID is not set or cannot be determined then an empty string will be returned.
	 *
	 * @param int $event_id
	 *
	 * @return string
	 */
	protected function fb_id( $event_id ) {
		return get_post_meta( $event_id, '_FacebookID', true );
	}
}