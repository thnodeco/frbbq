# Translation of Facebook Events in French (France)
# This file is distributed under the same license as the Facebook Events package.
msgid ""
msgstr ""
"PO-Revision-Date: 2015-06-09 14:27:35+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: GlotPress/1.0-alpha-1100\n"
"Project-Id-Version: Facebook Events\n"

#: src/Tribe/Importer.php:214
msgctxt "%s = human-readable time difference"
msgid "in about %s"
msgstr ""

#: src/Tribe/Importer.php:222
msgid "Next Import"
msgstr ""

#: src/Tribe/Importer.php:254
msgctxt "%s = human-readable time difference"
msgid "about %s ago"
msgstr ""

#: src/Tribe/Importer.php:262
msgid "Previous Import"
msgstr ""

#: src/Tribe/Importer.php:266
msgid "We couldn't find when the last auto-import happened, so the date above is an estimate."
msgstr ""

#: src/Tribe/Importer.php:401
msgid "the Add-Ons APIs settings page"
msgstr ""

#: src/Tribe/Importer.php:412
msgid "You must enter a Facebook App ID and App Secret to continue importing events from Facebook. Visit %1$s to create yours then head over to %2$s to apply them."
msgstr ""

#: src/Tribe/Resync.php:42
msgctxt "resync meta box title"
msgid "Facebook"
msgstr ""

#: src/Tribe/Resync.php:54
msgid "Re-synchronize"
msgstr ""

#: src/Tribe/Resync.php:55
msgid "If you wish to update this event to match any changes that may have been made over on facebook.com, please use the provided button."
msgstr ""

#: src/Tribe/Importer.php:135
msgid "Facebook Import Settings"
msgstr ""

#: src/Tribe/Importer.php:1143
msgid "%1$s. Event Image Error: \"%2$s\" is not a valid image. %3$s"
msgstr ""

#: src/admin-views/import-page.php:54
msgid "Select which events you want to import from specific Facebook Pages or Users by entering their details on the  %ssettings page%s. Return to this page to choose which of their events you would like to import."
msgstr ""

#: src/admin-views/import-page.php:56
msgid "Since you've already setup some Facebook organization(s) or page(s) to import from, you can import their events below. Visit the %ssettings page%s to modify the Facebook organization(s) or page(s) you want to import from."
msgstr ""

#: src/admin-views/import-page.php:59
msgid "You can determine an event's Facebook ID by looking at the URL of the event. For example, the ID of this event: %1$s would be %2$s"
msgstr ""

#: src/admin-views/import-page.php:68
msgid "Events from Facebook organizations or pages you've added:"
msgstr ""

#: src/Tribe/Importer.php:141
msgid "A page or organization's username or ID can be found in the URL used to access its profile. Modern Tribe's page is %s and the username is 'ModernTribeInc'. If a page or organization doesn't have a username, you will see the ID (numerical) in the URL."
msgstr ""

#. Plugin Name of the plugin/theme
msgid "The Events Calendar: Facebook Events"
msgstr ""

#. Description of the plugin/theme
msgid "Import events into The Events Calendar from a Facebook organization or page."
msgstr ""

#. Author of the plugin/theme
msgid "Modern Tribe, Inc."
msgstr ""

#. Author URI of the plugin/theme
msgid "http://m.tri.be/22"
msgstr ""

#: src/Tribe/Importer.php:369
msgid "As of version %s, you need to enter your own Facebook App ID and App secret. Visit %s to generate yours. Enter your App ID and App Secret on the <a href='%s'>event's settings page</a>. "
msgstr ""

#: src/Tribe/Importer.php:405
msgid "Signing up for a Facebook App ID and Secret only takes a second. Visit %s to create yours. "
msgstr "S'enregistrer pour un ID Facebook App et Secret ne prend qu'une seconde. Visitez %s pour créer le vôtre. "

#: src/Tribe/Importer.php:409
msgid "Visit %s to learn more about Facebook App Ids and Secrets. "
msgstr "Visitez %s pour apprendre plus à propos de l'App Facebook et Secret. "

#: src/Tribe/Importer.php:608
msgid "Could not successfully import the image for unknown reasons."
msgstr "Echec de l'importation d'image pour raisons inconnues."

#: src/Tribe/Importer.php:671
msgid "The return argument should be object or id"
msgstr "L'argument retourné devrait être un objet ou un id"

#: src/Tribe/Importer.php:747
msgid "Object type provided is invalid"
msgstr "Le type d'objet fournit est invalide"

#: src/Tribe/Importer.php:1035
msgid "Event ID \"%s\" was already imported from Facebook."
msgstr ""

#: src/Tribe/Importer.php:1045
msgid "Either the event with ID %s does not exist, is marked as private on Facebook or we couldn't reach the Facebook API"
msgstr "L'ID d'événement %s n'existe pas, ou est marqué comme étant privé sur Facebook, ou bien nous n'avons pas réussi à établir une connexion avec l'API de Facebook"

#: src/Tribe/Importer.php:1151
msgid "%s. Event Image Error: The file cannot be saved."
msgstr "%s. Erreur image événement: le fichier ne peut pas être sauvé. "

#: src/Tribe/Importer.php:1167
msgid "%s. Event Image Error: Failed to save record into database."
msgstr "%s. Erreur image événement: échec de sauvegarde dans la base de données. "

#: src/Tribe/Importer.php:1313
msgid "The Facebook event IDs provided must be numeric and one per line."
msgstr "Les IDs de l'événement Facebook doivent être numériques et introduits ligne par ligne."

#: src/Tribe/Importer.php:116 src/Tribe/Importer.php:317
#: src/Tribe/Importer.php:1622
msgid "Facebook"
msgstr "Facebook"

#: src/Tribe/Importer.php:1399
msgid "No valid events were provided for import. The import failed as a result."
msgstr "Aucun événement valide n'a été soumis pour l'importation. Echec de l'importation. "

#: src/Tribe/Importer.php:1429
msgctxt "%s is the name of the Facebook user or page"
msgid "Events from %s:"
msgstr "Evénements de %s:"

#: src/Tribe/Importer.php:1435
msgid "%s appears to be an individual. At this time events can only be fetched from organizations and pages, not individuals. If you would like to import an event from an individual, you can do so by entering the Facebook event ID in the box below."
msgstr ""

#: src/Tribe/Importer.php:1459
msgid "Events from <b>%s</b> are not importing because Facebook has returned an error, \"Unsupported get request.\" That error is usually due to an age or country restriction in that Facebook Page's settings, please see our %sFacebook API Errors%s article for options to resolve this."
msgstr ""

#: src/Tribe/Importer.php:1462
msgid "Facebook API Error returned for <b>%s</b>: %s"
msgstr ""

#: src/Tribe/Importer.php:1491
msgid "(previously imported)"
msgstr "(importé précédemment) "

#: src/Tribe/Importer.php:1504
msgctxt "%s is the name of the Facebook user or page"
msgid "%s does not have any Facebook events"
msgstr "%s ne contient pas d'événements Facebook"

#: src/Tribe/Importer.php:1592
msgid "Once Weekly"
msgstr "Une fois par semaine"

#: src/Tribe/Importer.php:1613
msgid "Import"
msgstr "Importation"

#: src/Tribe/Importer.php:1640
msgid "Settings"
msgstr "Configuration"

#: src/Tribe/Importer.php:1692
msgid "The Events Calendar"
msgstr "The Events Calendar"

#: src/Tribe/Importer.php:1693
msgid "To begin using The Events Calendar: Facebook Events, please install the latest version of %s."
msgstr "Afin d'utiliser The Events Calendar: Facebook Events, veuillez installer la dernière version de %s"

#: src/admin-views/import-page.php:34
msgid "Go take a look at your event(s)"
msgstr "Regardez vos événements"

#: src/admin-views/import-page.php:17
msgid "The following errors have occurred:"
msgstr "Les erreurs suivantes sont survenues:"

#: src/admin-views/import-page.php:24
msgid "Please note that as a result, no events were successfully imported."
msgstr "Veuillez noter qu'en conséquence, aucun événement n'as été importé avec succès."

#: src/admin-views/import-page.php:26
msgid "Please note that other events have been successfully imported."
msgstr "Veuillez noter que d'autres événements ont été importés avec succès."

#: src/admin-views/import-page.php:32
msgid "The selected event has been successfully imported."
msgid_plural "The %d selected events have been successfully imported."
msgstr[0] "L'événement sélectionné a été importé avec succès"
msgstr[1] "Les %d événements sélectionnés ont été importé avec succès"

#: src/admin-views/import-page.php:41
msgid "The following errors have occurred during importing images:"
msgstr "Les erreurs suivantes sont survenues lors de l'importation des images:"

#: src/admin-views/import-page.php:47
msgid "Please note that this does not effect importing of associated events unless noted."
msgstr "Veuillez noter que ceci n'affecte pas l'importation d'événements associés sauf notification."

#: src/admin-views/import-page.php:52
msgid "How to Import Facebook Events"
msgstr "Comment importer des événements Facebook "

#: src/admin-views/import-page.php:58
msgid "You can also import any specific event by entering Facebook event IDs in the text area below."
msgstr "vous pouvez également importer n'importe quel événement Facebook en introduisant l'ID dans le champ texte ci-dessous. "

#: src/admin-views/import-page.php:74
msgid "Import events by their Facebook ID:"
msgstr "Importer les événements Facebook sur base d'ID:"

#: src/admin-views/import-page.php:77
msgid "One event ID per line"
msgstr "Un événement pas ligne"

#: src/admin-views/import-page.php:82
msgid "Import events"
msgstr "Importer les événements"

#: src/admin-views/import-page.php:100
msgid "Please select or enter the ID of at least one event to import"
msgstr "Veuillez sélectionner ou introduire l'ID d'au moins un événement à importer"

#: src/Tribe/Importer.php:322
msgid "You need a Facebook App ID and App Secret to access data via the Facebook Graph API to import your events from Facebook."
msgstr "Vous avez besoin d'un ID App Facebook et App Secret afin de pouvoir accéder aux données via l'API Facebook Graph et de pouvoir importer à partir de Facebook."

#: src/Tribe/Importer.php:328 src/Tribe/Importer.php:337
msgid "Click here"
msgstr "Cliquez ici"

#: src/Tribe/Importer.php:140
msgid "You can retrieve and import events belonging to a Facebook organization or a Facebook page. You will need the username(s) or ID of each organization or page that you want to fetch events from. We do not currently support retrieving events from personal profiles. If you want to import an event from an individual, you can do that with the event ID on the"
msgstr "Vous pouvez récupérer et importer des événements Facebook par le biais de pages ou d'organisations. Vous devrez vous identifier avec votre nom d'utilisateur ou l'ID de chaque organisation ou page afin d'obtenir les événements. Nous ne fournissons pas de support pour la récupération d'événements provenant de profils personnels. Si vous voulez importer un événement d'un individuel, vous pouvez procéder en indiquant l'ID d'événement sur le "

#: src/Tribe/Importer.php:140
msgid "Import: Facebook page"
msgstr "Importation : Page Facebook "

#: src/Tribe/Importer.php:327
msgid "Facebook App ID"
msgstr "ID App Facebook"

#: src/Tribe/Importer.php:328
msgid "<p>%s to view or create your Facebook Apps"
msgstr "<p>%s pour voir ou créer vos Apps Facebook"

#: src/Tribe/Importer.php:336
msgid "Facebook App secret"
msgstr "Facebook App secret"

#: src/Tribe/Importer.php:337
msgid "<p>%s to view or create your App Secret"
msgstr "<p>%s pour voir ou créer votre App Secret"

#: src/Tribe/Importer.php:149
msgid "Organization and page usernames / IDs to fetch events from"
msgstr "Noms d'utilisateurs d'organisations ou de pages / ID's à récupérer des événements de "

#: src/Tribe/Importer.php:150
msgid "Please put one entry per line."
msgstr "Veuillez ajouter une entrée par ligne"

#: src/Tribe/Importer.php:150
msgid "Follow the instructions above to find usernames or IDs."
msgstr "Suivez les instructions ci-dessus pour trouver les noms d'utilisateurs et les IDs."

#: src/Tribe/Importer.php:150
msgid "Events can only be fetched from organizations and pages, not individuals."
msgstr "Les événements ne peuvent être récupérés qu'à partir d'organisations et de pages, pas à partir de personnes. "

#: src/Tribe/Importer.php:159
msgid "Default status to use for imported events"
msgstr "Statut par défaut à utiliser pour l'importation d'événements"

#: src/Tribe/Importer.php:166
msgid "Enable Google Maps on imported events"
msgstr "Activer Google Maps pour les événements importés"

#: src/Tribe/Importer.php:167
msgid "Check to enable maps for imported events in the frontend. Please enable Google Maps on the \"General\" tab to ensure your events will have map options."
msgstr "Vérifier l'activation des cartes pour les événements importés dans le front-end. Veuillez activer Google Maps dans le tab \"General\" afin de vous assurez que vos événements affichent bien les options de cartes. "

#: src/Tribe/Importer.php:174
msgid "Auto import from Facebook"
msgstr "Importation auto de Facebook "

#: src/Tribe/Importer.php:175
msgid "If selected, events will be automatically imported from Facebook at the set interval."
msgstr "Si sélectionné, les événements seront automatiquement importés de Facebook au temps initialisé."

#: src/Tribe/Importer.php:181
msgid "Import frequency"
msgstr "Fréquence d'importation"

#: src/Tribe/Importer.php:182
msgid "How often should we fetch events from Facebook. Only applies if Auto Import is set."
msgstr "A quelle fréquence faut-il récupérer les événements Facebook. Ceci ne s'applique que si l'Importation Auto a été sélectionnée. "

#: src/Tribe/Importer.php:183
msgid "Weekly"
msgstr "Hebdomadairement"

#: src/Tribe/Importer.php:183
msgid "Daily"
msgstr "Journellement"

#: src/Tribe/Importer.php:183
msgid "Twice daily"
msgstr "Deux fois par jour"

#: src/Tribe/Importer.php:183
msgid "Hourly"
msgstr "Chaque heure"