<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;



/**
 * AppGeo_Ajax class.
 *
 * creates ajax endpoints for checking in a user
 */
class AppGeo_Shortcode {


	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this->setup_actions();
	}


	/**
	 * setup_actions function.
	 *
	 * @access private
	 * @return void
	 */
	private function setup_actions() {
		add_action( 'init', array( $this, 'register_shortcodes' ) );
	}


	/**
	 * register_shortcodes function.
	 *
	 * @access public
	 * @return void
	 */
	public function register_shortcodes(){
		add_shortcode('checkin', array( $this, 'checkin') );
	}


	/**
	 * checkin function.
	 *
	 * Gets shortcode params and then echos a button.
	 *
	 * @access public
	 * @param mixed $atts
	 * @return void
	 */
	public function checkin( $atts ) {

		$default_btn_classes = 'btn btn-primary';

		$default_btn_classes = apply_filters( 'appgeo_button_class', $default_btn_classes );

		extract( $app_atts = shortcode_atts( array(
					'class'  => '', // button class for styling
					'title'	=> '',
					'address'	=> '',
					'place'	 => '',
					'button_text' =>  __( 'Check In', 'appgeo' ),
				), $atts ) );

		ob_start();
		?>
		<?php if ( AppPresser::is_app() ) { ?>

			<?php if ( is_user_logged_in() ) { 
			
			// TODO: Confirm new data attr work with both apv1 and apv2 to openCheckinModal()
			?>
				<button class="<?php echo $default_btn_classes; ?> onclick-appgeo-getloc <?php echo $app_atts['class']; ?>" id="checkin-here-btn" data-title="<?php echo $app_atts['title']; ?>" data-address="<?php echo $app_atts['address']; ?>" data-place="<?php echo $app_atts['place']; ?>">
					<?php echo $app_atts['button_text']; ?>
				</button>
			<?php } else { ?>
				<button class="<?php echo $default_btn_classes. ' ' . $app_atts['class']; ?>"
					onclick="AppGeo_login()">
					<?php echo $app_atts['button_text']; ?>
				</button>
			<?php } ?>

		<?php } ?>
		<?php
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}




}


function checkinButton( $atts = '' ) {
	if ( AppPresser::is_app() ) {
		echo do_shortcode('[checkin '. $atts .']');
	}
}