<?php
/**
 * Default config settings
 *
 * Enter any WordPress config settings that are default to all environments
 * in this file. These can then be overridden in the environment config files.
 * 
 * Please note if you add constants in this file (i.e. define statements) 
 * these cannot be overridden in environment config files.
 * 
 * @package    Studio 24 WordPress Multi-Environment Config
 * @version    1.0
 * @author     Studio 24 Ltd  <info@studio24.net>
 */
  

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+<hP-_B:qB|iU=J2v_fU)mYp.}e%8%pp9-;@w~+Ti.H:Sq([qZ?AX};xB(yQB$tX');
define('SECURE_AUTH_KEY',  '+%}aCaPMI$cFj9l{~~dV!*fm822n}sgm?[(#F-]IEPTL=T+]^B%SR|:-q_8D,jqI');
define('LOGGED_IN_KEY',    '(T[FfHFawk7.+:z_9&uz@}y_2.Plm5T^}2/+-y[I>K=pF2jBKfY^-J4%pe$PY<-+');
define('NONCE_KEY',        'J;vb!SR)v{ePv}tsRRFl1|$(65y2C<**<<w|?NV[)`YDib-na.j3-V++1I%L;gAk');
define('AUTH_SALT',        'jhlThAor~x+i{~ljBPZ=4WXYI/4#jw<xYp2j=s]pbqvS4;HjVLDWR4S`t~A[lT%^');
define('SECURE_AUTH_SALT', 'Tdq<E;)&RyC2uGf47c$xGALEPuZG#=&yB&--9RK,9^wA2fb8)T=?.AY+-5`^h8/b');
define('LOGGED_IN_SALT',   'E|-ovm|vxW>3>u#_BA0#AnQ<,n=uib+!cs?T3%a8q]8XQlP@F{_-ifI-$,~]+J-1');
define('NONCE_SALT',       'd9NY7e(.gYHEs/+vJ$#mjj7,uDmyN<x7,tMct8-D_)^R5xA1Fv5i*E)SARIqO:$+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * Increase memory limit. 
 */
define('WP_MEMORY_LIMIT', '128M');